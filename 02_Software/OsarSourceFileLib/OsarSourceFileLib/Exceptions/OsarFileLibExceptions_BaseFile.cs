﻿/* Used Namespaces -----------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.Exceptions.BaseFile
{
  /**
    * @brief      BaseFile Exception OsarFileLibException_InvalidFilePath
    */
  [Serializable]
  public class OsarFileLibException_InvalidFilePath : Exception
  {
    public OsarFileLibException_InvalidFilePath() { }
    public OsarFileLibException_InvalidFilePath(string message) : base(message) { }
    public OsarFileLibException_InvalidFilePath(string message, Exception inner) : base(message, inner) { }
    protected OsarFileLibException_InvalidFilePath(SerializationInfo info, StreamingContext context) : base(info, context) { }
  }

  /**
    * @brief      BaseFile Exception OsarFileLibException_InvalidFileName
    */
  public class OsarFileLibException_InvalidFileName : Exception
  {
    public OsarFileLibException_InvalidFileName() { }
    public OsarFileLibException_InvalidFileName(string message) : base(message) { }
    public OsarFileLibException_InvalidFileName(string message, Exception inner) : base(message, inner) { }
    protected OsarFileLibException_InvalidFileName(SerializationInfo info, StreamingContext context) : base(info, context) { }
  }
}
