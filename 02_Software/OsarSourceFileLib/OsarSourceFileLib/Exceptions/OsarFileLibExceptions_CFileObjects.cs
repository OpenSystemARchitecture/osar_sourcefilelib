﻿/* Used Namespaces -----------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.Exceptions.CFileObjects
{
  /**
    * @brief      CFileObjects   Exception OsarFileLibException_GroupNotAvailable
    */
  public class OsarFileLibException_GroupNotAvailable : Exception
  {
    public OsarFileLibException_GroupNotAvailable() { }
    public OsarFileLibException_GroupNotAvailable(string message) : base(message) { }
    public OsarFileLibException_GroupNotAvailable(string message, Exception inner) : base(message, inner) { }
    protected OsarFileLibException_GroupNotAvailable(SerializationInfo info, StreamingContext context) : base(info, context) { }
  }
}
