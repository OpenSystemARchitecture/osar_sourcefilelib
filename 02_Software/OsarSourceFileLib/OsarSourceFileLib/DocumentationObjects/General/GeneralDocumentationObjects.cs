﻿/* Used Namespaces -----------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarSourceFileLib.DocumentationObjects.Doxygen;

/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.DocumentationObjects.General
{
  /**
    * @brief      General Object "GeneralStartGroupObject"
    * @note       This class is the realization of the a general start group object.
    * @details    Used Interfaces:
    *             >> DocumentationObject
    */
  public class GeneralStartGroupObject : DocumentationObject
  {
    private string customStartGroupString;
    private string customGroupName;
    private string osarMemMap;
    private string osarMemMapModuleName;
    private OsarMemMapDataType osarMemMapType;

    /**
      * @brief      Constructor
      */
    public GeneralStartGroupObject()
    {
      customStartGroupString = "";
      customGroupName = "";
      osarMemMap = "";
      osarMemMapModuleName = "";
      osarMemMapType = OsarMemMapDataType.OSAR_CODE;
    }

    /**
      * @brief      Destructor
      */
    ~GeneralStartGroupObject()
    { }

    /**
      * @brief      User function to add an custom start group string
      * @param      string customGroupStartString
      * @retval     None
      * @note       None
      */
    public void AddCustomStartGroupString(string customGroupStartString)
    {
      customStartGroupString = customGroupStartString;
    }

    /**
      * @brief      User function to add an general group name
      * @param      string customGroupNameString
      * @retval     None
      * @note       None
      */
    public void AddGroupName(string customGroupNameString)
    {
      customGroupName = customGroupNameString;
    }

    /**
      * @brief      User function to add a OSAR Start mem map define.
      * @param      string module name
      * @param      OsarMemMapDataType type of data
      * @retval     none
      * @note       None
      */
    public void AddOsarStartMemMapDefine(string moduleName, OsarMemMapDataType dataType)
    {
      osarMemMap = "";
      switch (dataType)
      {
        case OsarMemMapDataType.OSAR_CODE:
          osarMemMap += "#define " + moduleName + "_START_SEC_CODE\n";
          osarMemMap += "#include \"" + moduleName + "_MemMap.h\"\n";
        break;

        case OsarMemMapDataType.OSAR_CONST:
          osarMemMap += "#define " + moduleName + "_START_SEC_CONST\n";
          osarMemMap += "#include \"" + moduleName + "_MemMap.h\"\n";
        break;

        case OsarMemMapDataType.OSAR_INIT_VAR:
          osarMemMap += "#define " + moduleName + "_START_SEC_INIT_VAR\n";
          osarMemMap += "#include \"" + moduleName + "_MemMap.h\"\n";
        break;

        case OsarMemMapDataType.OSAR_NOINIT_VAR:
          osarMemMap += "#define " + moduleName + "_START_SEC_NOINIT_VAR\n";
          osarMemMap += "#include \"" + moduleName + "_MemMap.h\"\n";
        break;

        case OsarMemMapDataType.OSAR_ZERO_VAR:
          osarMemMap += "#define " + moduleName + "_START_SEC_ZERO_INIT_VAR\n";
          osarMemMap += "#include \"" + moduleName + "_MemMap.h\"\n";
        break;

        default:
          throw new NotImplementedException("Actual MemMap type unknown or not implemented. MemMap type: " + dataType.ToString());
        break;
      }
    }

    /**
      * @brief      User function to request the OSAR moudle name used for mem map define.
      * @retval     string module name
      */
    public string GetOsarModuleName()
    {
      return osarMemMapModuleName;
    }

    /**
      * @brief      User function to request the OSAR memory type used for mem map define.
      * @retval     OsarMemMapDataType memory data type
      */
    public OsarMemMapDataType GetOsarMemMapType()
    {
      return osarMemMapType;
    }


    /**
      * @brief      Interface function to start the string generation
      * @param      None
      * @retval     None
      * @detail     Generate the general start group information
      */
    public string GenerateString()
    {
      string completeStringElement = "";
      string tempStringElement = "";
      int lengthOfGroupName = customGroupName.Length;
      int cntOfCommentLength = Convert.ToInt32(DoxygenComment.DoxygenCntOfCharsPerLine);
      int cntOfSingleLength = ( (cntOfCommentLength- lengthOfGroupName ) / 2);

      if ((lengthOfGroupName+8) >= cntOfCommentLength)
      {
        completeStringElement += "/*- " + customGroupName + "-*/\n";
      }
      else
      {
        tempStringElement += "/*" + string.Concat(Enumerable.Repeat('-', cntOfSingleLength - 3)) + " " + customGroupName + " " + string.Concat(Enumerable.Repeat('-', cntOfSingleLength - 3)) + "*/\n";
        if(tempStringElement.Length < cntOfCommentLength)
        {
          char[] tempCharArray = { '-' };
          tempStringElement.CopyTo(0, tempCharArray, 3, 1);
        }
        completeStringElement += tempStringElement;
      }

      /* Add Osar Mem Map start string */
      completeStringElement += osarMemMap;

      /* Add Custom start group string */
      if (customStartGroupString != "")
        customStartGroupString += "\n";
      completeStringElement += customStartGroupString;
      return completeStringElement;
    }

    /**
      * @brief      Interface function to check if selected element is start element of the string
      * @param      string to be checked
      * @retval     bool >> true = element is start element of string
      */
    public bool IsParseStringStartObject(string str)
    {
      bool retVal;
      string searchPattern;
      int indexOfSearchPattern;

      searchPattern = "/*-";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = true;
      }
      else
      {
        retVal = false;
      }

      return retVal;
    }

    /**
      * @brief      Interface function to start the string parser.
      * @param      string to be parsed
      * @retval     count of chars which has been parsed
      * @detail     Parse the source string using all added elements.
      */
    public int ParseString(string str)
    {
      string searchPattern, tmpString, tmpString2, workingString;
      string[] tmpStringArray;
      int indexOfSearchPattern, cnt = 0, processedChars = 0;
      bool breakProcessing = false;

      /* >>>> Parse start group comment <<<< */
      workingString = str;
      searchPattern = "/*-";
      indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
      processedChars += indexOfSearchPattern + searchPattern.Length;
      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        tmpString = string.Empty;

        // Search for group name
        indexOfSearchPattern = workingString.IndexOf(" ");
        workingString = workingString.Remove(0, workingString.IndexOf(" ") + 1);
        processedChars += indexOfSearchPattern + 1;

        string customGroupName = workingString.Substring(0, workingString.IndexOf(" "));

        indexOfSearchPattern = workingString.IndexOf("\n");
        workingString = workingString.Remove(0, workingString.IndexOf("\n") + 1);
        processedChars += indexOfSearchPattern + 1;

        /* >>>> Parse Grouping information <<<< */
        /* Check if Memory Mapping is grouping information */
        searchPattern = "_START_SEC_";
        indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
        if (0 <= indexOfSearchPattern )
        {
          searchPattern = "#define ";
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);

          /* Memory Mapping found >> Pase Memory mapping */
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
          processedChars += indexOfSearchPattern + searchPattern.Length;

          tmpStringArray = workingString.Split('_');
          tmpString = string.Empty;
          tmpString2 = string.Empty;

          for (int idx = 0; idx < tmpStringArray.Length - 2; idx++)
          {
            if (( "START" == tmpStringArray[idx] ) && ( "SEC" == tmpStringArray[idx + 1] ))
            {
              tmpStringArray = tmpStringArray[idx + 2].Split('\n');
              tmpString2 = "OSAR_" + tmpStringArray[0];
              break;
            }
            else
            {
              if (string.Empty == tmpString)
              {
                tmpString += tmpStringArray[idx];
              }
              else
              {
                tmpString += "_" + tmpStringArray[idx];
              }
            }
          }

          /* Store data */
          osarMemMapModuleName = tmpString;
          osarMemMapType = (OsarMemMapDataType)Enum.Parse(typeof(OsarMemMapDataType), tmpString2, true);
          AddOsarStartMemMapDefine(osarMemMapModuleName, osarMemMapType);

          /* Update processed elements */
          searchPattern = "\n";
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
          cnt += indexOfSearchPattern + 1;
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
          cnt += indexOfSearchPattern + 1;

          processedChars += cnt;
        }

        /* >>>>> Parse custom comment <<<<< */
        if (true == workingString.Contains("/* "))
        {
          indexOfSearchPattern = workingString.IndexOf("/* ");
          if(0 == indexOfSearchPattern)
          {
            // Custom start string detected
            workingString = workingString.Remove(0, indexOfSearchPattern + 3);
            processedChars += indexOfSearchPattern + 3;

            indexOfSearchPattern = workingString.IndexOf(" */\n");
            customStartGroupString = workingString.Substring(0, indexOfSearchPattern);
            workingString = workingString.Remove(0, indexOfSearchPattern + 4);
            processedChars += indexOfSearchPattern + 4;
          }
        }
      }
      else
      {
        processedChars = 0;
      }

      return processedChars;
    }
  }

  /**
    * @brief      General Object "GeneralEndGroupObject"
    * @note       This class is the realization of the a general end group object.
    * @details    Used Interfaces:
    *             >> DocumentationObject
    */
  public class GeneralEndGroupObject : DocumentationObject
  {
    private string customEndGroupString;
    private string osarMemMap;
    private string osarMemMapModuleName;
    private OsarMemMapDataType osarMemMapType;

    /**
      * @brief      Constructor
      */
    public GeneralEndGroupObject()
    {
      customEndGroupString = "";
      osarMemMap = "";
    }

    /**
      * @brief      Destructor
      */
    ~GeneralEndGroupObject()
    { }

    /**
      * @brief      User function to add a custom end group string
      * @param      string customGroupEndString
      * @retval     None
      * @note       None
      */
    public void AddCustomEndGroupString(string customGroupEndString)
    {
      customEndGroupString = customGroupEndString + "\n";
    }

    /**
      * @brief      User function to add a OSAR Stop mem map define.
      * @param      string module name
      * @param      OsarMemMapDataType type of data
      * @retval     none
      * @note       None
      */
    public void AddOsarStopMemMapDefine(string moduleName, OsarMemMapDataType dataType)
    {
      osarMemMap = "";
      switch (dataType)
      {
        case OsarMemMapDataType.OSAR_CODE:
          osarMemMap += "#define " + moduleName + "_STOP_SEC_CODE\n";
          osarMemMap += "#include \"" + moduleName + "_MemMap.h\"\n";
        break;

        case OsarMemMapDataType.OSAR_CONST:
          osarMemMap += "#define " + moduleName + "_STOP_SEC_CONST\n";
          osarMemMap += "#include \"" + moduleName + "_MemMap.h\"\n";
        break;

        case OsarMemMapDataType.OSAR_INIT_VAR:
          osarMemMap += "#define " + moduleName + "_STOP_SEC_INIT_VAR\n";
          osarMemMap += "#include \"" + moduleName + "_MemMap.h\"\n";
        break;

        case OsarMemMapDataType.OSAR_NOINIT_VAR:
          osarMemMap += "#define " + moduleName + "_STOP_SEC_NOINIT_VAR\n";
          osarMemMap += "#include \"" + moduleName + "_MemMap.h\"\n";
        break;

        case OsarMemMapDataType.OSAR_ZERO_VAR:
          osarMemMap += "#define " + moduleName + "_STOP_SEC_ZERO_INIT_VAR\n";
          osarMemMap += "#include \"" + moduleName + "_MemMap.h\"\n";
        break;

        default:
          throw new NotImplementedException("Actual MemMap type unknown or not implemented. MemMap type: " + dataType.ToString());
        break;
      }
    }

    /**
      * @brief      Interface funtion to start the string generation
      * @param      None
      * @retval     None
      * @detail     Generate the general end group information
      */
    public string GenerateString()
    {

      return (osarMemMap + customEndGroupString);
    }

    /**
      * @brief      Interface function to check if selected element is start element of the string
      * @param      string to be checked
      * @retval     bool >> true = element is start element of string
      */
    public bool IsParseStringStartObject(string str)
    {
      bool retVal;
      string searchPatternCustomString, searchPatternMemMap;
      int indexOfSearchPatternCustomString, indexOfSearchPatternMemMap;

      searchPatternCustomString = "/* ";
      indexOfSearchPatternCustomString = str.IndexOf(searchPatternCustomString, 0, str.Length, StringComparison.Ordinal);

      searchPatternMemMap = "_STOP_SEC_";
      indexOfSearchPatternMemMap = str.IndexOf(searchPatternMemMap, 0, str.Length, StringComparison.Ordinal);
      if ((0 <= indexOfSearchPatternMemMap ) || ( 0 <= indexOfSearchPatternCustomString ))
      {
        /* Element Found at start pos*/
        retVal = true;
      }
      else
      {
        retVal = false;
      }

      return retVal;
    }

    /**
      * @brief      Interface function to start the string parser.
      * @param      string to be parsed
      * @retval     count of chars which has been parsed
      * @detail     Parse the source string using all added elements.
      */
    public int ParseString(string str)
    {
      string searchPattern, tmpString, tmpString2, workingString;
      string[] tmpStringArray;
      int indexOfSearchPattern, cnt = 0, processedChars = 0;

      workingString = str;

      /* >>>> Parse Grouping information <<<< */
      /* Check if Memory Mapping is grouping information */
      searchPattern = "_STOP_SEC_";
      indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
      if (0 <= indexOfSearchPattern)
      {
        searchPattern = "#define ";
        indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);

        /* Memory Mapping found >> Pase Memory mapping */
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
        processedChars += indexOfSearchPattern + searchPattern.Length;

        tmpStringArray = workingString.Split('_');
        tmpString = string.Empty;
        tmpString2 = string.Empty;

        for (int idx = 0; idx < tmpStringArray.Length - 2; idx++)
        {
          if (( "STOP" == tmpStringArray[idx] ) && ( "SEC" == tmpStringArray[idx + 1] ))
          {
            tmpStringArray = tmpStringArray[idx + 2].Split('\n');
            tmpString2 = "OSAR_" + tmpStringArray[0];
            break;
          }
          else
          {
            if (string.Empty == tmpString)
            {
              tmpString += tmpStringArray[idx];
            }
            else
            {
              tmpString += "_" + tmpStringArray[idx];
            }
          }
        }

        /* Store data */
        osarMemMapModuleName = tmpString;
        osarMemMapType = (OsarMemMapDataType)Enum.Parse(typeof(OsarMemMapDataType), tmpString2, true);
        AddOsarStopMemMapDefine(osarMemMapModuleName, osarMemMapType);

        /* Update processed elements */
        searchPattern = "\n";
        indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
        cnt += indexOfSearchPattern + 1;
        indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
        cnt += indexOfSearchPattern + 1;

        processedChars += cnt;
      }

      /* >>>>> Parse custom comment <<<<< */
      if (true == workingString.Contains("/* "))
      {
        indexOfSearchPattern = workingString.IndexOf("/* ");
        if (0 == indexOfSearchPattern)
        {
          // Custom start string detected
          workingString = workingString.Remove(0, indexOfSearchPattern + 3);
          processedChars += indexOfSearchPattern + 3;

          indexOfSearchPattern = workingString.IndexOf(" */\n");
          customEndGroupString = workingString.Substring(0, indexOfSearchPattern);
          workingString = workingString.Remove(0, indexOfSearchPattern + 4);
          processedChars += indexOfSearchPattern + 4;
        }
      }

      return processedChars;
    }
  }

  /**
    * @brief      Enumeration of OSAR Mem Map definitions
    */
  public enum OsarMemMapDataType
  {
    OSAR_CODE,
    OSAR_NOINIT_VAR,
    OSAR_INIT_VAR,
    OSAR_ZERO_VAR,
    OSAR_CONST
  };

}
