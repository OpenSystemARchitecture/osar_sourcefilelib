///////////////////////////////////////////////////////////
//  DoxygenElementDescription.cs
//  Implementation of the Class DoxygenElementDescription
//  Generated by Enterprise Architect
//  Created on:      03-Feb-2018 14:59:09
//  Original author: SRein
///////////////////////////////////////////////////////////
/* Used Namespaces -----------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using OsarSourceFileLib.DocumentationObjects;
using System.Linq;
/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.DocumentationObjects.Doxygen {
  /**
    * @brief      Doxygen Object "DoxygenElementDescription"
    * @note       This class is used to impelement an doxygen element description.
    * @details    Used Interfaces:
    *             >> DocumentationObject
    */
  public class DoxygenElementDescription : DocumentationObject {

    private string elementBrief;
    private string elementDetails;
    private string elementNote;
    private List<string> elementParam;
    private string elementRetVal;
    private string elementUUID;

    /**
      * @brief      Constructor
      */
    public DoxygenElementDescription(){
      elementBrief = "";
      elementDetails = "";
      elementNote = "";
      elementRetVal = "";
      elementUUID = "";
      elementParam = new List<string>();
    }

    /**
      * @brief      Destructor
      */
    ~DoxygenElementDescription(){

		}

    /**
      * @brief      User function to add file details information
      * @param      string details
      * @retval     None
      * @note       None
      */
    public void AddElementDetails(string details){
      elementDetails = details;

    }

    /**
      * @brief      User function to add file brief information
      * @param      string brief
      * @retval     None
      * @note       None
      */
    public void AddElementBrief(string brief){
      elementBrief = brief;

    }

    /**
      * @brief      User function to add file note information
      * @param      string note
      * @retval     None
      * @note       None
      */
    public void AddElementNote(string note){
      elementNote = note;
    }

    /**
      * @brief      User function to add file parameter information
      * @param      string param
      * @param      direction information
      * @retval     None
      * @note       None
      */
    public void AddElementParameter(string param, CParameterType direction){
      switch (direction)
      {
        case CParameterType.INPUT:
        elementParam.Add("[in]       " + param);
        break;

        case CParameterType.OUTPUT:
        elementParam.Add("[out]      " + param);
        break;

        case CParameterType.INPUT_OUTPUT:
        elementParam.Add("[in,out]   " + param);
        break;

        default:
        elementParam.Add("           " + param);
        break;
      }

    }

    /**
      * @brief      User function to add return value information
      * @param      string returnValue
      * @retval     None
      * @note       None
      */
    public void AddElementReturnValues(string returnValue){
      elementRetVal = returnValue;
    }

    /**
      * @brief      User function to add a "Universal Unique ID" (UUID)
      * @param      string uuid
      * @retval     None
      * @note       Id shall be a "Guid" (Globally Unique Identifier) >> But it must not be one
      */
    public void AddElementUUID(string uuid)
    {
      elementUUID = uuid;
    }

    /**
      * @brief      User function to return the "Universal Unique ID" (UUID)
      * @param      None
      * @retval     string UUID
      */
    public string GetElementUUID()
    {
      return elementUUID;
    }

    /**
      * @brief      User function to generate a Universal Unique ID" (UUID)
      * @param      None
      * @retval     string UUID
      * @note       The generated UUID would automatically be set internal as UUID.
      *             The generated UUID is of type "Guid" (Globally Unique Identifier).
      */
    public string GenerateElementUUID()
    {
      string uuid;
      uuid = Guid.NewGuid().ToString("N");

      /* Use internal interface to add UUID */
      AddElementUUID(uuid);

      /* Use internal interface to get the UUID */
      return GetElementUUID();
    }


    /**
      * @brief      Interface funtion to start the string generation
      * @param      None
      * @retval     None
      * @detail     Generate the doxygen element description
      */
    public string GenerateString(){
      string completeStringElement = "";

      completeStringElement += DoxygenComment.DoxygenCommentStart + "\n";

      if (elementBrief != "")
      {
        completeStringElement += DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenBriefTag + "    " + RearrangeUserStringsToFittIntoTextField(elementBrief) + "\n";
      }

      if (0 != elementParam.Count)
      {
        for (int idx = 0; idx < elementParam.Count; idx++)
        {
          completeStringElement += DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenParamTag + RearrangeUserStringsToFittIntoTextField(elementParam[idx]) + "\n";
        }
      }

      if (elementRetVal != "")
      {
        completeStringElement += DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenRetvalTag + RearrangeUserStringsToFittIntoTextField(elementRetVal) + "\n";
      }
      if (elementDetails != "")
      {
        completeStringElement += DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenDetailsTag + "    " + RearrangeUserStringsToFittIntoTextField(elementDetails) + "\n";
      }
      if (elementNote != "")
      {
        completeStringElement += DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenNoteTag + "    " + RearrangeUserStringsToFittIntoTextField(elementNote) + "\n";
      }
      if (elementUUID != "")
      {
        completeStringElement += DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenUUIDTag + elementUUID + "\n";
      }

      completeStringElement += DoxygenComment.DoxygenCommentEnd + "\n";

      return completeStringElement;
		}

    /// @brief  Function to rearrange the comitted text into the Documentation header
    /// <param name="usrString"></param>
    /// @retval string with rearranged content
    private string RearrangeUserStringsToFittIntoTextField(string userString)
    {
      char[] splittGenericChar = { '\n' };
      char[] splittSubChar = { ' ' };
      string[] usrStringArray;
      string[] tempStringArray;
      Int32 startCnt, initStartCnt, cntOfCaratersNeeded;
      string tempStringElement = "", completeString = "";
      Boolean firstLineWritten = false;

      /* Rearrange user string */
      tempStringElement = userString.Replace("\r\n", "\n");
      usrStringArray = tempStringElement.Split(splittGenericChar);
      tempStringElement = "";

      /* Generic rearrange parameters*/
      Int32 cntOfElementsPerLine = Convert.ToInt32(DoxygenComment.DoxygenCntOfCharsPerLine);
      startCnt = Convert.ToInt32(DoxygenComment.DoxygenCntofCharsFromElementStartToString) + 4;
      initStartCnt = DoxygenComment.DoxygenGenericElementStartString.Length - DoxygenComment.DoxygenMiddleCommentStartString.Length + Convert.ToInt32(DoxygenComment.DoxygenCntofCharsFromElementStartToString);

      /* Check if user string is small enougth */
      if (( userString.Length <= ( cntOfElementsPerLine - startCnt - 2 ) ) && ( usrStringArray.Length == 1 ))
      {
        /* User String is small enougth >> Nothin todo */
        return userString;
      }



      for (int idx = 0; idx < usrStringArray.Length; idx++)
      {
        /* Splitt current string element on spaces to rebuilt the string again */
        tempStringArray = usrStringArray[idx].Split(splittSubChar);

        for (int idx2 = 0; idx2 < tempStringArray.Length; idx2++)
        {
          /* Check if first element has already been written */
          if (false == firstLineWritten)
          {
            /* Mainline will be processed here */
            if (( tempStringElement + tempStringArray[idx2] ).Length >= ( cntOfElementsPerLine - initStartCnt - 3 ))
            {
              /* New Element is too large >> Fill with spaces */
              completeString += tempStringElement;

              tempStringElement = DoxygenComment.DoxygenMiddleCommentStartString + string.Concat(Enumerable.Repeat(' ', startCnt)) + tempStringArray[idx2];

              if (idx2 < (tempStringArray.Length -1))
              { tempStringElement += ' '; }

              firstLineWritten = true;
            }
            else
            {
              tempStringElement += tempStringArray[idx2] + ' ';
            }
          }
          else
          {
            /* Sublines will be processed here */
            if (( tempStringElement + tempStringArray[idx2] ).Length >= ( cntOfElementsPerLine - 3 ))
            {
              /* New Element is too large >> Fill with spaces */
              completeString += "\n" + tempStringElement;
              tempStringElement = DoxygenComment.DoxygenMiddleCommentStartString + string.Concat(Enumerable.Repeat(' ', startCnt)) + tempStringArray[idx2];// + ' ';
            }
            else
            {
              tempStringElement += tempStringArray[idx2] + ' ';
            }
          }
        }

        /* Remove atefacts */
        if (' ' == tempStringElement[tempStringElement.Length-1])
        {
          tempStringElement = tempStringElement.Remove(tempStringElement.Length - 1, 1);
        }
        if(0 < tempStringElement.Length)
        {
          if (' ' == tempStringElement[tempStringElement.Length - 1])
          {
            tempStringElement = tempStringElement.Remove(tempStringElement.Length - 1, 1);
          }
        }

        /* Fill last element with spaces */
        if (true == firstLineWritten)
        {
          completeString += "\n" + tempStringElement;
          tempStringElement = DoxygenComment.DoxygenMiddleCommentStartString + string.Concat(Enumerable.Repeat(' ', startCnt));
        }
        else
        {
          completeString += tempStringElement;
          tempStringElement = DoxygenComment.DoxygenMiddleCommentStartString + string.Concat(Enumerable.Repeat(' ', startCnt));
          firstLineWritten = true;
        }
      }

      /* Remove atefacts */
      completeString = completeString.Replace(" \n", "\n");

      return completeString;
    }


    /**
      * @brief      Interface function to check if selected element is start element of the string
      * @param      string to be checked
      * @retval     bool >> true = element is start element of string
      */
    public bool IsParseStringStartObject(string str)
    {
      bool retVal;
      string searchPattern;
      int indexOfSearchPattern;

      searchPattern = "/**\n * @";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = true;
      }
      else
      {
        retVal = false;
      }

      return retVal;
    }

    /**
      * @brief      Interface function to start the string parser.
      * @param      string to be parsed
      * @retval     count of chars which has been parsed
      * @detail     Parse the source string using all added elements.
      */
    public int ParseString(string str)
    {
      string searchPattern, workingString, tmpString;
      int indexOfSearchPattern, processedChars = 0;

      /* Step 1 > Search for end pattern and cut all behind */
      workingString = str;
      searchPattern = DoxygenComment.DoxygenCommentEnd + "\n";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);

      if (0 != indexOfSearchPattern)
      {
        /* Remove all behinde the end string*/
        str = str.Remove(indexOfSearchPattern, str.Length - indexOfSearchPattern);
        processedChars += searchPattern.Length + indexOfSearchPattern;
      }


      /* Step 2 > Search for brief name */
      workingString = str;
      searchPattern = DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenBriefTag + "    ";
      indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (-1 != indexOfSearchPattern)
      {
        /* Element found >> Continue with processing */
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
        elementBrief = reconstructContentFromRearrangedString(workingString);
      }


      /* Step 3 > Search for parameter in */
      workingString = str;
      searchPattern = DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenParamTag + "[in]       ";
      indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (-1 != indexOfSearchPattern)
      {
        /* Element found >> Continue with processing */
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
        tmpString = reconstructContentFromRearrangedString(workingString);
        AddElementParameter(tmpString, CParameterType.INPUT);
      }


      /* Step 4 > Search for parameter out */
      workingString = str;
      searchPattern = DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenParamTag + "[out]      ";
      indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (-1 != indexOfSearchPattern)
      {
        /* Element found >> Continue with processing */
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
        tmpString = reconstructContentFromRearrangedString(workingString);
        AddElementParameter(tmpString, CParameterType.OUTPUT);
      }

      /* Step 5 > Search for parameter in out */
      workingString = str;
      searchPattern = DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenParamTag + "[in,out]   ";
      indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (-1 != indexOfSearchPattern)
      {
        /* Element found >> Continue with processing */
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
        tmpString = reconstructContentFromRearrangedString(workingString);
        AddElementParameter(tmpString, CParameterType.INPUT_OUTPUT);
      }


      /* Step 5 > Search for return value */
      workingString = str;
      searchPattern = DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenRetvalTag;
      indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (-1 != indexOfSearchPattern)
      {
        /* Element found >> Continue with processing */
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
        elementRetVal = reconstructContentFromRearrangedString(workingString);
      }


      /* Step 6 > Search for details */
      workingString = str;
      searchPattern = DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenDetailsTag + "    ";
      indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (-1 != indexOfSearchPattern)
      {
        /* Element found >> Continue with processing */
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
        elementDetails = reconstructContentFromRearrangedString(workingString);
      }

      /* Step 6 > Search for notes */
      workingString = str;
      searchPattern = DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenNoteTag + "    ";
      indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (-1 != indexOfSearchPattern)
      {
        /* Element found >> Continue with processing */
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
        elementNote = reconstructContentFromRearrangedString(workingString);
      }

      /* Step 6 > Search for uuid */
      workingString = str;
      searchPattern = DoxygenComment.DoxygenGenericElementStartString + DoxygenComment.DoxygenUUIDTag;
      indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (-1 != indexOfSearchPattern)
      {
        /* Element found >> Continue with processing */
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
        elementUUID = reconstructContentFromRearrangedString(workingString);
      }

      return processedChars;

    }

    /**
      * @brief      Internal helper function to pars the string data from rearanged string
      * @param      string  rearranged user string
      * @retval     string  user string
      */
    private string reconstructContentFromRearrangedString(string str)
    {
      bool lastElementSpace = false, lastElementNewLine = false, firstElementFound = false;
      string tmpString = string.Empty;

      foreach (char c in str)
      {
        /* Delete first spaces */
        if (false == firstElementFound)
        {
          if (' ' == c)
          {
            // Do Nothing >> Deleting first spaces
          }
          else
          {
            /* First character found */
            firstElementFound = true;
            tmpString += c;
          }
        }
        else
        {
          /* Check for comment end character */
          if (( '*' == c ))// || ( '\n' == c ))
          { /* Do nothing */ }
          else
          {
            /* Check for next tag character */
            if ('@' == c)
            { break; }
            else
            {
              tmpString += c;
            }
          }
        }
      }

      /* Remove double spaces with \r\n */
      tmpString = tmpString.Replace("  ", "");
      tmpString = tmpString.Replace("\n ", "\n");

      lastElementSpace = tmpString.EndsWith(" ");
      lastElementNewLine = tmpString.EndsWith("\n");
      if ((true == lastElementSpace) || (true == lastElementNewLine ))
      {
        /* Remove last caracter */
        tmpString = tmpString.Remove(tmpString.Length - 1, 1);
      }
      return tmpString;

    }

  }//end DoxygenElementDescription

}//end namespace Doxygen