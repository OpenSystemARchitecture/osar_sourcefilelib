/*****************************************************************************************************************************
 * @file        CFileObjectFunctionInterface.cs                                                                              *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        15.02.2018 11:08:24                                                                                          *
 * @brief       CFileObject >> Function Interface                                                                            *
*****************************************************************************************************************************/
/* Used Namespaces -----------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.CFile.CFileObjects {
  /**
    * @brief      Interface for function objects
    */
  public interface CFileObjectFunctionInterface  {

		string GenerateFunctionPrototypeString();
	}//end CFileObjectFunctionInterface

}//end namespace CFileObjects