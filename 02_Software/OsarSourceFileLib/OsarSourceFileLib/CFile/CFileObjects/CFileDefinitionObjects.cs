﻿/*****************************************************************************************************************************
 * @file        CFileDefinitionObjects.cs                                                                                    *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        15.02.2018 11:08:24                                                                                          *
 * @brief       CFileObject >> Define object class.                                                                          *
 * @details     This class is used to implement an C-Define class.                                                           *
*****************************************************************************************************************************/
/* Used Namespaces -----------------------------------------------------------*/
using OsarSourceFileLib.DocumentationObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarSourceFileLib.DocumentationObjects.General;
using OsarSourceFileLib.DocumentationObjects.Doxygen;

/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.CFile.CFileObjects
{
  /**
  * @brief      CFile Define-Object
  * @note       This class is used to realize C-Define functionalities of an CFile Object 
  * @details    Base class:
  *             >> CFileBaseObject
  */
  public class CFileDefinitionObjects : CFileBaseObject
  {
    private int maxLengthOfDefine = 0;
    private string[] defines = { "#define ", "" , "", ""};
    /**
      * @brief      Constructor
      */
    public CFileDefinitionObjects() : base()
    {
    }

    /**
      * @brief      Destructor
      */
    ~CFileDefinitionObjects()
    {
    }

    /**
      * @brief      User function to add an single define without any grouping or description information
      * @param      string  define name
      * @param      string  define value
      * @retval     None
      * @note       None
      */
    public void AddDefinition(string definitionName, string definitionValue)
    {
      checkLengthAndFillContent(definitionName, definitionValue);

      AddCFileObject(defines);
    }

    /**
      * @brief      User function to add an single define without any grouping but with description information
      * @param      string  define name
      * @param      string  define name
      * @param      DocumentationObject  Define Description 
      * @retval     None
      * @note       None
      */
    public void AddDefinition(string definitionName, string definitionValue, DocumentationObject defineDescription)
    {
      checkLengthAndFillContent(definitionName, definitionValue);

      AddCFileObject(defines, defineDescription);
    }

    /**
      * @brief      User function to add an define within an defined group but without description information
      * @param      string  define name
      * @param      string  define value
      * @param      int Index of the group which shall be used.
      * @retval     None
      * @note       An group as to be created first:
      *             Exceptions:
      *               >> OsarFileLibException_GroupNotAvailable
      */
    public void AddDefinition(string definitionName, string definitionValue, int groupIdx)
    {
      checkLengthAndFillContent(definitionName, definitionValue);

      AddCFileObject(defines, groupIdx);
    }

    /**
      * @brief      User function to add an define within an defined group and with description information
      * @param      string  define name
      * @param      string  define value
      * @param      int Index of the group which shall be used.
      * @param      DocumentationObject with define description
      * @retval     None
      * @note       An group as to be created first:
      *             Exceptions:
      *               >> OsarFileLibException_GroupNotAvailable
      */
    public void AddDefinition(string definitionName, string definitionValue, int groupIdx, DocumentationObject defineDescription)
    {
      checkLengthAndFillContent(definitionName, definitionValue);

      AddCFileObject(defines, groupIdx, defineDescription);
    }

    /**
      * @brief      Internal helper function to get an equal spaces between definition and value
      * @param      string  define name
      * @param      string  define value
      * @retval     None
      * @note       Function will fill automatical the object private "defines" variable
      */
    private void checkLengthAndFillContent(string definitionName, string definitionValue)
    {
      int spaceLength = 0;
      string spaces = "";
      defines[1] = definitionName;
      defines[3] = definitionValue;

      /* Check definition length and calculate space length */
      if (definitionName.Length > maxLengthOfDefine)
      {
        maxLengthOfDefine = definitionName.Length;

        /* Update Base Elmement length */
        updateBaseClassElementContentWithNewMaxSpaceLength();

        spaceLength = 1;
      }
      else
      {
        spaceLength = maxLengthOfDefine - definitionName.Length + 1;
      }


      /* Create space element */
      spaces = string.Concat(Enumerable.Repeat(' ', spaceLength));
      defines[2] = spaces;
    }

    /**
      * @brief      Internal helper function to update the base class space length between definition and value
      * @param      None
      * @retval     None
      */
    private void updateBaseClassElementContentWithNewMaxSpaceLength()
    {
      int spaceLength = 0;
      string spaces = "";

      /* Step 1 >> Update Single Elements */
      for (int idx = 0; idx < singleObjectList.Count; idx++)
      {
        spaceLength = maxLengthOfDefine - singleObjectList[idx].objectString[1].Length + 1;   //First element in objectString == Definition Name
        spaces = string.Concat(Enumerable.Repeat(' ', spaceLength));
        singleObjectList[idx].objectString[2] = spaces;                                       //Second element in objectString == Spaces between Name and Value
      }

      /* Step 2 >> Update Group Elements Elements */
      for (int idx = 0; idx < objectListWithGroups.Count; idx++)
      {

        for(int idx2 = 0; idx2 < objectListWithGroups[idx].objects.Count; idx2++)
        {
          spaceLength = maxLengthOfDefine - objectListWithGroups[idx].objects[idx2].objectString[1].Length + 1;     //First element in objectString == Definition Name
          spaces = string.Concat(Enumerable.Repeat(' ', spaceLength));
          objectListWithGroups[idx].objects[idx2].objectString[2] = spaces;                                         //Second element in objectString == Spaces between Name and Value
        }
      }
    }

    /**
      * @brief      Interface function to start the string parser.
      * @param      string to be parsed
      * @retval     parse status >> true = parse sucessful
      * @detail     Parse the source string using all added elements.
      */
    public override bool ParseString(string str)
    {
      bool parseSuccessful;
      bool isStartString = false, groupCloserFound = false, groupOpenerFound = false, elementProcessed = false;
      string workingString = str, searchPattern;
      int processedChars, groupIndex, indexOfSearchPattern;
      GeneralStartGroupObject functionGroupOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject functionGroupCloser = new GeneralEndGroupObject();
      DoxygenElementDescription doxElementDescriptor = new DoxygenElementDescription();

      /* >>>>>>>>>> Start Parsing of the string <<<<<<<<<< */
      while (0 < workingString.Length)
      {
        /* Step 01: Check if elements are grouped */
        isStartString = functionGroupOpener.IsParseStringStartObject(workingString);
        if (true == isStartString)
        {
          groupOpenerFound = true;
          processedChars = functionGroupOpener.ParseString(workingString);

          /* Remove grouping information from working string */
          workingString = workingString.Remove(0, processedChars);
        }

        /* >>>> Process grouped elements <<<< */
        if (true == groupOpenerFound)
        {
          elementProcessed = false;

          /* Create group closer */
          functionGroupCloser.AddOsarStopMemMapDefine(functionGroupOpener.GetOsarModuleName(), functionGroupOpener.GetOsarMemMapType());
          groupIndex = AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);

          while (false == groupCloserFound)
          {
            /* Step 02: Check for documentation */
            doxElementDescriptor = new DoxygenElementDescription();
            isStartString = doxElementDescriptor.IsParseStringStartObject(workingString);
            if (true == isStartString)
            {
              elementProcessed = true;
              processedChars = doxElementDescriptor.ParseString(workingString);

              /* Remove documentation information from working string */
              workingString = workingString.Remove(0, processedChars);
            }


            /* Step 03: Parse variable elements */
            isStartString = IsParseStringStartDefinitionObject(workingString);
            if (true == isStartString)
            {
              /* Check if doxygen comment is available */
              if (true == elementProcessed)
              {
                processedChars = ParseDefinitonString(workingString, groupIndex, doxElementDescriptor);
              }
              else
              {
                processedChars = ParseDefinitonString(workingString, groupIndex, null);
              }


              /* Remove element information from working string */
              workingString = workingString.Remove(0, processedChars);
              elementProcessed = true;
            }



            /* Check of one element has been processed */
            if (false == elementProcessed)
            {
              groupCloserFound = true;
            }
            else
            {
              elementProcessed = false;
            }
          }

          /* Remove Group Closer from working string (two lines)*/
          searchPattern = "\n";
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);

          /* Reset Grouping Flags */
          groupOpenerFound = false;
          groupCloserFound = false;
        }
        else
        {
          /* Process Include strings without grouping information */
          elementProcessed = true;
          while (true == elementProcessed)
          {
            elementProcessed = false;

            /* Step 02: Check for documentation */
            doxElementDescriptor = new DoxygenElementDescription();
            isStartString = doxElementDescriptor.IsParseStringStartObject(workingString);
            if (true == isStartString)
            {
              elementProcessed = true;
              processedChars = doxElementDescriptor.ParseString(workingString);

              /* Remove documentation information from working string */
              workingString = workingString.Remove(0, processedChars);
            }

            /* Step 03: Parse variable elements */
            isStartString = IsParseStringStartDefinitionObject(workingString);
            if (true == isStartString)
            {
              /* Check if doxygen comment is available */
              if (true == elementProcessed)
              {
                processedChars = ParseDefinitonString(workingString, -1, doxElementDescriptor);
              }
              else
              {
                processedChars = ParseDefinitonString(workingString, -1, null);
              }


              /* Remove element information from working string */
              workingString = workingString.Remove(0, processedChars);
              elementProcessed = true;
            }

            /* Check of one element has been processed */
            if (true == elementProcessed)
            {
              elementProcessed = true;
            }
            else
            {
              elementProcessed = false;
            }
          }
        }
      }

      /* Check if parsing was successful */
      if (0 == workingString.Length)
      { parseSuccessful = true; }
      else
      { parseSuccessful = false; }

      return parseSuccessful;
    }

    /**
      * @brief      Interface function to check if selected element is start element of the string
      * @param      string to be checked
      * @retval     bool >> true = element is start element of string
      * @note       A direct detection if the object is the actual start element is not possible. So an
      *             indirect detection must be used.
      */
    public bool IsParseStringStartDefinitionObject(string str)
    {
      bool retVal;
      int indexOfSearchPattern;
      string searchPattern, workingString;

      searchPattern = "#define ";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        /* Check if MemMap Definiton has been found >> Stop Section */
        searchPattern = "\n";
        indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
        workingString = str.Remove(indexOfSearchPattern);

        if(true == workingString.Contains("_STOP_SEC_"))
        {
          /* An MemMap end section has been found */
          retVal = false;
        }
        else
        {
          /* A standard definiton has been found */
          retVal = true;
        }
        
      }
      else
      {
        retVal = false;
      }

      return retVal;
    }

    /**
      * @brief      Interface function to start the string parser for an variable string.
      * @param      string to be parsed
      * @param      int group id
      * @param      DocumentationObject variableDescription
      * @retval     Count of chars which has been parsed
      */
    private int ParseDefinitonString(string str, int groupId, DocumentationObject variableDescription)
    {
      string searchPattern, workingString;
      int indexOfSearchPattern, processedChars = 0, idx;
      char[] splitPattern = { ' ' };
      string[] stringElements;
      string defName = "", defValue = "";

      /* Search for end pattern */
      searchPattern = "\n";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      workingString = str.Substring(0, indexOfSearchPattern);
      processedChars += workingString.Length + searchPattern.Length;

      /* Split elements */
      stringElements = workingString.Split(splitPattern);

      /* Parse definiton name */
      defName = stringElements[1];


      /* Parse definiton content */
      for(idx = 2; idx < stringElements.Length; idx++)
      {

        if("" != stringElements[idx])
        {
          if ("" == defValue)
          {
            defValue = stringElements[idx];
          }
          else
          {
            defValue += " " + stringElements[idx];
          }
        }
        
      }

      /* Add definiton information */
      if (-1 == groupId)
      {
        if (null == variableDescription)
        {
          AddDefinition(defName, defValue);
        }
        else
        {
          AddDefinition(defName, defValue, variableDescription);
        }
      }
      else
      {
        if (null == variableDescription)
        {
          AddDefinition(defName, defValue, groupId);
        }
        else
        {
          AddDefinition(defName, defValue, groupId, variableDescription);
        }
      }

      return processedChars;
    }

  }
}
