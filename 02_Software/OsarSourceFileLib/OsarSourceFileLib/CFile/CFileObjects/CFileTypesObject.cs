/*****************************************************************************************************************************
 * @file        CFileTypeObjects.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        16.02.2018 11:08:24                                                                                          *
 * @brief       CFileObject >> Type object class.                                                                            *
 * @details     This class is used to implement an types class.                                                              *
*****************************************************************************************************************************/
/* Used Namespaces -----------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using OsarSourceFileLib.DocumentationObjects.General;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.DocumentationObjects;
using OsarSourceFileLib.CFile.CFileObjects;
/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.CFile.CFileObjects {
  /**
    * @brief      CFile Types-Object
    * @note       This class is used to realize C-Types functionalities of an CFile Object 
    * @details    Base class:
    *             >> CFileBaseObject
    */
  public class CFileTypeObjects : CFileBaseObject {
                                       /*   0    , 1 ,  2 , 3 ,  4 */
    private string[] baseDataTypes = { "typedef ", "", " ", "", ";\n" };                            //0 == Typedef elements
                                                                                                    //1 == Base data type
                                                                                                    //2 == " " Space between data types
                                                                                                    //3 == New data type
                                                                                                    //4 == ";" Closer
                                      /*      0      ,   1  , 2 ,   3  , 4 ,  5 */
    private string[] enumDataTypes = { "typedef enum", "{\n", "", "\n}", "", ";\n" };               //0 == Typedef enumeration
                                                                                                    //1 == Enum element opener
                                                                                                    //2 == Enum elements
                                                                                                    //3 == Enum element closer
                                                                                                    //4 == Enum type name
                                                                                                    //5 == ";" Closer

    /*      0      ,   1  , 2 ,   3  , 4 ,  5 */
    private string[] structDataTypes = { "typedef struct", "{\n", "", "\n}", "", ";\n" };           //0 == Typedef struct
                                                                                                    //1 == Struct element opener
                                                                                                    //2 == Struct elements
                                                                                                    //3 == Struct element closer
                                                                                                    //4 == Struct type name
                                                                                                    //5 == ";" Closer

    /**
      * @brief      Constructor
      */
    public CFileTypeObjects() : base()
    {
		}

    /**
      * @brief      Destructor
      */
    ~CFileTypeObjects(){

		}


    /**
      * @brief      User function to add a single type definition without any grouping or description information
      * @param      string  base data type
      * @param      string  new data type name
      * @retval     None
      * @note       None
      */
    public void AddNewDataTypeDefinition(string baseDataType, string newDataType)
    {
      baseDataTypes[1] = baseDataType;
      baseDataTypes[3] = newDataType;

      AddCFileObject(baseDataTypes);
    }

    /**
      * @brief      User function to add a single type definition without any grouping but with description information
      * @param      string  base data type
      * @param      string  new data type name
      * @param      string  data type description
      * @retval     None
      * @note       None
      */
    public void AddNewDataTypeDefinition(string baseDataType, string newDataType, DocumentationObject dataTypeDescription)
    {
      baseDataTypes[1] = baseDataType;
      baseDataTypes[3] = newDataType;

      AddCFileObject(baseDataTypes, dataTypeDescription);
    }

    /**
      * @brief      User function to add a single type definition with grouping but without description information
      * @param      string  base data type
      * @param      string  new data type name
      * @param      int     group of data type
      * @retval     None
      * @note       None
      */
    public void AddNewDataTypeDefinition(string baseDataType, string newDataType, int groupIdx)
    {
      baseDataTypes[1] = baseDataType;
      baseDataTypes[3] = newDataType;

      AddCFileObject(baseDataTypes, groupIdx);
    }

    /**
      * @brief      User function to add a single type definition with grouping and with description information
      * @param      string  base data type
      * @param      string  new data type name
      * @param      int     group of data type
      * @param      string  data type description
      * @retval     None
      * @note       None
      */
    public void AddNewDataTypeDefinition(string baseDataType, string newDataType, int groupIdx, DocumentationObject dataTypeDescription)
    {
      baseDataTypes[1] = baseDataType;
      baseDataTypes[3] = newDataType;

      AddCFileObject(baseDataTypes, groupIdx, dataTypeDescription);
    }

    /*--------------------------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------------------------------------------------------------------------------------*/

    /**
      * @brief      User function to add a single enumeration definition without any grouping or description information
      * @param      string    Enumeration type name
      * @param      string[]  Array with enumeration elements
      * @retval     None
      * @note       None
      */
    public void AddEnumerationTypeDefinition(string enumName, string[] enumElements)
    {
      enumDataTypes[2] = rearrangeEnumerationElements(enumElements);
      enumDataTypes[4] = enumName;

      AddCFileObject(enumDataTypes);
    }

    /**
      * @brief      User function to add a single enumeration definition without any grouping but with description information
      * @param      string    Enumeration type name
      * @param      string[]  Array with enumeration elements
      * @param      string    data type description
      * @retval     None
      * @note       None
      */
    public void AddEnumerationTypeDefinition(string enumName, string[] enumElements, DocumentationObject enumDescription)
    {
      enumDataTypes[2] = rearrangeEnumerationElements(enumElements);
      enumDataTypes[4] = enumName;

      AddCFileObject(enumDataTypes, enumDescription);
    }

    /**
      * @brief      User function to add a single enumeration definition with grouping but without description information
      * @param      string    Enumeration type name
      * @param      string[]  Array with enumeration elements
      * @param      int       group of data type
      * @retval     None
      * @note       None
      */
    public void AddEnumerationTypeDefinition(string enumName, string[] enumElements, int groupIdx)
    {
      enumDataTypes[2] = rearrangeEnumerationElements(enumElements);
      enumDataTypes[4] = enumName;

      AddCFileObject(enumDataTypes, groupIdx);
    }

    /**
      * @brief      User function to add a single enumeration definition with grouping and with description information
      * @param      string    Enumeration type name
      * @param      string[]  Array with enumeration elements
      * @param      int       group of data type
      * @param      string    data type description
      * @retval     None
      * @note       None
      */
    public void AddEnumerationTypeDefinition(string enumName, string[] enumElements, int groupIdx, DocumentationObject enumDescription)
    {
      enumDataTypes[2] = rearrangeEnumerationElements(enumElements);
      enumDataTypes[4] = enumName;

      AddCFileObject(enumDataTypes, groupIdx, enumDescription);
    }

    /*--------------------------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------------------------------------------------------------------------------------*/

    /**
      * @brief      User function to add a single struct definition without any grouping or description information
      * @param      string    Struct type name
      * @param      string[]  Array with struct elements
      * @retval     None
      * @note       None
      */
    public void AddStructTypeDefinition(string structName, string[] structElements)
    {
      structDataTypes[2] = rearrangeStructElements(structElements);
      structDataTypes[4] = structName;

      AddCFileObject(structDataTypes);

    }

    /**
      * @brief      User function to add a single struct definition without any grouping but with description information
      * @param      string    Struct type name
      * @param      string[]  Array with struct elements
      * @param      string    data type description
      * @retval     None
      * @note       None
      */
    public void AddStructTypeDefinition(string structName, string[] structElements, DocumentationObject structDescription)
    {
      structDataTypes[2] = rearrangeStructElements(structElements);
      structDataTypes[4] = structName;

      AddCFileObject(structDataTypes, structDescription);
    }

    /**
      * @brief      User function to add a single struct definition with grouping but without description information
      * @param      string    Struct type name
      * @param      string[]  Array with struct elements
      * @param      int       group of data type
      * @retval     None
      * @note       None
      */
    public void AddStructTypeDefinition(string structName, string[] structElements, int groupIdx)
    {
      structDataTypes[2] = rearrangeStructElements(structElements);
      structDataTypes[4] = structName;

      AddCFileObject(structDataTypes, groupIdx);
    }

    /**
      * @brief      User function to add a single struct definition with grouping and with description information
      * @param      string    Struct type name
      * @param      string[]  Array with struct elements
      * @param      int     group of data type
      * @param      string    data type description
      * @retval     None
      * @note       None
      */
    public void AddStructTypeDefinition(string structName, string[] structElements, int groupIdx, DocumentationObject structDescription)
    {
      structDataTypes[2] = rearrangeStructElements(structElements);
      structDataTypes[4] = structName;

      AddCFileObject(structDataTypes, groupIdx, structDescription);
    }

    /*--------------------------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------------------------------------------------------------------------------------*/
    /**
      * @brief      Internal helper function to aline the enumeration elements
      * @param      string[] String array with enumeration elements
      * @retval     string  Rearranged elements
      * @note       None
      */
    private string rearrangeEnumerationElements(string[] enumElements)
    {
      string tempString = "";

      for(int idx = 0; idx < enumElements.Length; idx++)
      {
        if(idx == (enumElements.Length-1))
        {
          tempString += "  " + enumElements[idx];
        }
        else
        {
          tempString += "  " + enumElements[idx] + ",\n";
        }
      }

      return tempString;
    }

    /**
      * @brief      Internal helper function to aline the struct elements
      * @param      string[] String array with struct elements
      * @retval     string  Rearranged elements
      * @note       None
      */
    private string rearrangeStructElements(string[] structElements)
    {
      string tempString = "";

      for (int idx = 0; idx < structElements.Length; idx++)
      {
        if (idx == ( structElements.Length - 1 ))
        {
          tempString += "  " + structElements[idx] + ";";
        }
        else
        {
          tempString += "  " + structElements[idx] + ";\n";
        }
      }

      return tempString;
    }

    /**
      * @brief      Interface function to start the string parser.
      * @param      string to be parsed
      * @retval     parse status >> true = parse sucessful
      * @detail     Parse the source string using all added elements.
      */
    public override bool ParseString(string str)
    {
      bool parseSuccessful;
      bool isStartString = false, groupCloserFound = false, groupOpenerFound = false, elementProcessed = false;
      string workingString = str, searchPattern;
      int processedChars, groupIndex, indexOfSearchPattern;
      GeneralStartGroupObject functionGroupOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject functionGroupCloser = new GeneralEndGroupObject();
      DoxygenElementDescription doxElementDescriptor = new DoxygenElementDescription();


      /* >>>>>>>>>> Start Parsing of the string <<<<<<<<<< */
      while (0 < workingString.Length)
      {
        /* Step 01: Check if elements are grouped */
        isStartString = functionGroupOpener.IsParseStringStartObject(workingString);
        if (true == isStartString)
        {
          groupOpenerFound = true;
          processedChars = functionGroupOpener.ParseString(workingString);

          /* Remove grouping information from working string */
          workingString = workingString.Remove(0, processedChars);
        }

        /* >>>> Process grouped elements <<<< */
        if (true == groupOpenerFound)
        {
          elementProcessed = false;

          /* Create group closer */
          functionGroupCloser.AddOsarStopMemMapDefine(functionGroupOpener.GetOsarModuleName(), functionGroupOpener.GetOsarMemMapType());
          groupIndex = AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);

          while (false == groupCloserFound)
          {
            /* Step 02: Check for documentation */
            doxElementDescriptor = new DoxygenElementDescription();
            isStartString = doxElementDescriptor.IsParseStringStartObject(workingString);
            if (true == isStartString)
            {
              elementProcessed = true;
              processedChars = doxElementDescriptor.ParseString(workingString);

              /* Remove documentation information from working string */
              workingString = workingString.Remove(0, processedChars);
            }


            /* Step 03: Parse variable elements */
            isStartString = IsParseStringStartTypeObject(workingString);
            if (true == isStartString)
            {
              /* Check if doxygen comment is available */
              if (true == elementProcessed)
              {
                processedChars = ParseTypesString(workingString, groupIndex, doxElementDescriptor);
              }
              else
              {
                processedChars = ParseTypesString(workingString, groupIndex, null);
              }


              /* Remove element information from working string */
              processedChars++; // Remove also the additional '\n' after each variable
              workingString = workingString.Remove(0, processedChars);
              elementProcessed = true;
            }



            /* Check of one element has been processed */
            if (false == elementProcessed)
            {
              groupCloserFound = true;
            }
            else
            {
              elementProcessed = false;
            }
          }

          /* Remove Group Closer from working string (two lines)*/
          searchPattern = "\n";
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);

          /* Reset Grouping Flags */
          groupOpenerFound = false;
          groupCloserFound = false;
        }
        else
        {
          /* Process Include strings without grouping information */
          elementProcessed = true;
          while (true == elementProcessed)
          {
            elementProcessed = false;

            /* Step 02: Check for documentation */
            doxElementDescriptor = new DoxygenElementDescription();
            isStartString = doxElementDescriptor.IsParseStringStartObject(workingString);
            if (true == isStartString)
            {
              elementProcessed = true;
              processedChars = doxElementDescriptor.ParseString(workingString);

              /* Remove documentation information from working string */
              workingString = workingString.Remove(0, processedChars);
            }

            /* Step 03: Parse variable elements */
            isStartString = IsParseStringStartTypeObject(workingString);
            if (true == isStartString)
            {
              /* Check if doxygen comment is available */
              if (true == elementProcessed)
              {
                processedChars = ParseTypesString(workingString, -1, doxElementDescriptor);
              }
              else
              {
                processedChars = ParseTypesString(workingString, -1, null);
              }


              /* Remove element information from working string */
              processedChars++; // Remove also the additional '\n' after each variable
              workingString = workingString.Remove(0, processedChars);
              elementProcessed = true;
            }

            /* Check of one element has been processed */
            if (true == elementProcessed)
            {
              elementProcessed = true;
            }
            else
            {
              elementProcessed = false;
            }
          }
        }
      }

      /* Check if parsing was successful */
      if (0 == workingString.Length)
      { parseSuccessful = true; }
      else
      { parseSuccessful = false; }

      return parseSuccessful;
    }

    /**
      * @brief      Interface function to check if selected element is start element of the string
      * @param      string to be checked
      * @retval     bool >> true = element is start element of string
      */
    public bool IsParseStringStartTypeObject(string str)
    {
      bool retVal;
      string searchPattern;
      int indexOfSearchPattern;

      searchPattern = "typedef ";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = true;
      }
      else
      {
        retVal = false;
      }

      return retVal;
    }

    /**
      * @brief      Interface function to start the string parser for an variable string.
      * @param      string to be parsed
      * @param      int group id
      * @param      DocumentationObject variableDescription
      * @retval     Count of chars which has been parsed
      * @note       Function detects the data type definition type and calls the equal sub function
      */
    private int ParseTypesString(string str, int groupId, DocumentationObject variableDescription)
    {
      bool exist;
      string searchPattern, workingString;
      int indexOfSearchPattern, processedChars = 0;

      /* Check Type of variable data to be parsed */
      searchPattern = ";\n\n";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      workingString = str.Substring(0, indexOfSearchPattern);
      processedChars += searchPattern.Length - 1;

      /* Check type of data */
      searchPattern = "typedef enum{\n";
      exist = workingString.Contains(searchPattern);

      if(true == exist)
      {
        /* Enumeration type detected */
        processedChars += ParseEnumTypeString(workingString, groupId, variableDescription);
      }
      else
      {
        searchPattern = "typedef struct{\n";
        exist = workingString.Contains(searchPattern);

        if (true == exist)
        {
          /* Struct type detected */
          processedChars += ParseStructTypeString(workingString, groupId, variableDescription);
        }
        else
        {
          /* Standard type detected */
          processedChars += ParseStdTypeString(workingString, groupId, variableDescription);
        }
      }

      return processedChars;
    }


    /**
      * @brief      Interface function to start the string parser for an standard type definition string.
      * @param      string to be parsed
      * @param      int group id
      * @param      DocumentationObject variableDescription
      * @retval     Count of chars which has been parsed
      */
    private int ParseStdTypeString(string str, int groupId, DocumentationObject variableDescription)
    {
      char[] splitPattern = { ' ' };
      string[] stringElements;
      string baseDataType = "", newDataType = "";


      /* Split elements */
      stringElements = str.Split(splitPattern);

      /* Process base data type */
      baseDataType = stringElements[1];

      /* Process new data type */
      newDataType = stringElements[2];

      /* Add type information */
      if (-1 == groupId)
      {
        if (null == variableDescription)
        {
          AddNewDataTypeDefinition(baseDataType, newDataType);
        }
        else
        {
          AddNewDataTypeDefinition(baseDataType, newDataType, variableDescription);
        }
      }
      else
      {
        if (null == variableDescription)
        {
          AddNewDataTypeDefinition(baseDataType, newDataType, groupId);
        }
        else
        {
          AddNewDataTypeDefinition(baseDataType, newDataType, groupId, variableDescription);
        }
      }

      return str.Length;
    }


    /**
      * @brief      Interface function to start the string parser for an enumeration type definition string.
      * @param      string to be parsed
      * @param      int group id
      * @param      DocumentationObject variableDescription
      * @retval     Count of chars which has been parsed
      */
    private int ParseEnumTypeString(string str, int groupId, DocumentationObject variableDescription)
    {
      char[] splitPattern = { '\n' };
      string[] stringElements, enumElements;
      List<string> enumElementList = new List<string>();
      string tempString = "", enumName = "", searchPattern, workingString;
      int indexOfSearchPattern;

      /* Remove first line */
      searchPattern = "typedef enum{\n";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0);
      workingString = str.Remove(0, indexOfSearchPattern + searchPattern.Length);

      /* Process all enumeration values */
      /* Split elements */
      stringElements = workingString.Split(splitPattern);

      foreach (string prcStr in stringElements)
      {
        if (false == prcStr.Contains("}"))
        {
          /* Process enumeration values */
          tempString = prcStr.Replace("  ", "");
          tempString = tempString.Replace(",", "");
          enumElementList.Add(tempString);
        }
        else
        {
          enumName = prcStr.Replace("}", "");
          enumName = enumName.Replace(";", "");
        }
      }

      enumElements = enumElementList.ToArray();

      /* Add variable information */
      if (-1 == groupId)
      {
        if (null == variableDescription)
        {
          AddEnumerationTypeDefinition(enumName, enumElements);
        }
        else
        {
          AddEnumerationTypeDefinition(enumName, enumElements, variableDescription);
        }
      }
      else
      {
        if (null == variableDescription)
        {
          AddEnumerationTypeDefinition(enumName, enumElements, groupId);
        }
        else
        {
          AddEnumerationTypeDefinition(enumName, enumElements, groupId, variableDescription);
        }
      }

      return str.Length;
    }

    /**
      * @brief      Interface function to start the string parser for an struct type definition string.
      * @param      string to be parsed
      * @param      int group id
      * @param      DocumentationObject variableDescription
      * @retval     Count of chars which has been parsed
      */
    private int ParseStructTypeString(string str, int groupId, DocumentationObject variableDescription)
    {
      char[] splitPattern = { '\n' };
      string[] stringElements, structElements;
      List<string> structElementList = new List<string>();
      string tempString = "", structName = "", searchPattern, workingString;
      int indexOfSearchPattern;

      /* Remove first line */
      searchPattern = "typedef struct{\n";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0);
      workingString = str.Remove(0, indexOfSearchPattern + searchPattern.Length);

      /* Process all enumeration values */
      /* Split elements */
      stringElements = workingString.Split(splitPattern);

      foreach (string prcStr in stringElements)
      {
        if (false == prcStr.Contains("}"))
        {
          /* Process enumeration values */
          tempString = prcStr.Replace("  ", "");
          tempString = tempString.Replace(";", "");
          structElementList.Add(tempString);
        }
        else
        {
          structName = prcStr.Replace("}", "");
          structName = structName.Replace(";", "");
        }
      }

      structElements = structElementList.ToArray();

      /* Add variable information */
      if (-1 == groupId)
      {
        if (null == variableDescription)
        {
          AddStructTypeDefinition(structName, structElements);
        }
        else
        {
          AddStructTypeDefinition(structName, structElements, variableDescription);
        }
      }
      else
      {
        if (null == variableDescription)
        {
          AddStructTypeDefinition(structName, structElements, groupId);
        }
        else
        {
          AddStructTypeDefinition(structName, structElements, groupId, variableDescription);
        }
      }

      return str.Length;
    }
  }//end CFileTypesObject

}//end namespace CFileObjects