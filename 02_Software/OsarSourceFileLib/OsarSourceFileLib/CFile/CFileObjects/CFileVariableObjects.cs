/*****************************************************************************************************************************
 * @file        CFileVariableObjects.cs                                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        16.02.2018 11:08:24                                                                                          *
 * @brief       CFileObject >> Variable object class.                                                                        *
 * @details     This class is used to implement an variable class.                                                           *
*****************************************************************************************************************************/
/* Used Namespaces -----------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using OsarSourceFileLib.DocumentationObjects.General;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.DocumentationObjects;
using OsarSourceFileLib.CFile.CFileObjects;

using System.Linq;
/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.CFile.CFileObjects {
  public enum arrayRearrangeType { AUTO_DETECT, STD_ARRAY, STRUCT_ARRAY }

  /**
    * @brief      CFile Variable-Object
    * @note       This class is used to realize C-Variable functionalities of an CFile Object 
    * @details    Base class:
    *             >> CFileBaseObject
    */
  public class CFileVariableObjects : CFileBaseObject {
                                /* 0 , 1 ,  2 , 3 ,  4*/
    private string[] variables = { "", "", " ", "", ";\n"};                                   //0 == Access modifier
                                                                                              //1 == Data type
                                                                                              //2 == Space between Data Type an variable name
                                                                                              //3 == variable name
                                                                                              //4 == ";" Closer
                                    /* 0 , 1 ,  2 , 3 ,   4,   5 ,  6 */
    private string[] initVariables = { "", "", " ", "", " = ", "", ";\n" };                   //0 == Access modifier
                                                                                              //1 == Data type
                                                                                              //2 == Space between Data Type an variable name
                                                                                              //3 == variable name
                                                                                              //4 == " = " Operator
                                                                                              //5 == init value
                                                                                              //6 == ";" Closer
                             /* 0 , 1 , 2 , 3 ,  4,  5 ,  6 ,  7,*/
    private string[] arrays = { "", "", " ", "", "[", "", "]", ";\n" };                        //0 == Access modifier
                                                                                              //1 == Data type
                                                                                              //2 == Space between Data Type an variable name
                                                                                              //3 == variable name
                                                                                              //4 == "[" Length opener
                                                                                              //5 == length infomration
                                                                                              //6 == "]" Length closer
                                                                                              //7 == ";" Closer
                                 /* 0 , 1 , 2 , 3 ,  4,  5 ,  6 ,    7,     8,     9 */
    private string[] initArrays = { "", "", " ", "", "[", "", "]", " = {\n", "", "\n};\n"};    //0 == Access modifier
                                                                                              //1 == Data type
                                                                                              //2 == Space between Data Type an variable name
                                                                                              //3 == variable name
                                                                                              //4 == "[" Length opener
                                                                                              //5 == length infomration
                                                                                              //6 == "]" Length closer
                                                                                              //7 == " = {\n" Init value opener
                                                                                              //8 == Init values
                                                                                              //9 == "\n};" Closer
    /**
      * @brief      Constructor
      */
    public CFileVariableObjects() : base()
    {

		}

    /**
      * @brief      Destructor
      */
    ~CFileVariableObjects(){

		}

    /**
      * @brief      User function to add a single array without any grouping or description information
      * @param      string  array data type
      * @param      string  array name
      * @param      string  length of the array
      * @param      string  array init value >> default none
      * @param      string  array access modifier >> default none 
      * @param      arrayRearrangeType >> Rearrange Type
      * @retval     None
      * @note       None
      */
    public void AddArray(string dataType, string variableName, string arrayLength, string variableInitValue = "", string accessModifier = "", arrayRearrangeType arrayType = arrayRearrangeType.AUTO_DETECT)
    {
      /* Check if an init value is needed */
      if ("" == variableInitValue)
      {
        /* >>>>> Use no init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          arrays[0] = accessModifier + " ";
        }
        else
        {
          arrays[0] = accessModifier;
        }

        arrays[1] = dataType;
        arrays[3] = variableName;
        arrays[5] = arrayLength;
        AddCFileObject(arrays);
      }
      else
      {
        /* >>>>> Use init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          initArrays[0] = accessModifier + " ";
        }
        else
        {
          initArrays[0] = accessModifier;
        }

        initArrays[1] = dataType;
        initArrays[3] = variableName;
        initArrays[5] = arrayLength;


        /* Check Rearrange type */
        switch (arrayType)
        {
          case arrayRearrangeType.AUTO_DETECT:
          /* Check if init string contains an "{" >> Chooes struct init */
          if(variableInitValue.Contains("{"))
          {
            initArrays[8] = rearrangeStructArrayInitValues(variableInitValue);
          }
          else
          {
            initArrays[8] = rearrangeArrayInitValues(variableInitValue);
          }
          break;

          case arrayRearrangeType.STD_ARRAY:
          initArrays[8] = rearrangeArrayInitValues(variableInitValue);
          break;

          case arrayRearrangeType.STRUCT_ARRAY:
          initArrays[8] = rearrangeStructArrayInitValues(variableInitValue);
          break;

          default:
            throw new ArgumentOutOfRangeException("Array rearrange type is out of range.");
        }

        AddCFileObject(initArrays);
      }
    }

    /**
      * @brief      User function to add a single array without any grouping but with description information
      * @param      string  array data type
      * @param      string  array name
      * @param      string  length of the array
      * @param      DocumentationObject  array description
      * @param      string  array init value >> default none
      * @param      string  array access modifier >> default none 
      * @param      arrayRearrangeType >> Rearrange Type
      * @retval     None
      * @note       None
      */
    public void AddArray(string dataType, string variableName, string arrayLength, DocumentationObject variableDescription, string variableInitValue = "", string accessModifier = "", arrayRearrangeType arrayType = arrayRearrangeType.AUTO_DETECT)
    {
      /* Check if an init value is needed */
      if ("" == variableInitValue)
      {
        /* >>>>> Use no init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          arrays[0] = accessModifier + " ";
        }
        else
        {
          arrays[0] = accessModifier;
        }

        arrays[1] = dataType;
        arrays[3] = variableName;
        arrays[5] = arrayLength;
        AddCFileObject(arrays, variableDescription);
      }
      else
      {
        /* >>>>> Use init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          initArrays[0] = accessModifier + " ";
        }
        else
        {
          initArrays[0] = accessModifier;
        }

        initArrays[1] = dataType;
        initArrays[3] = variableName;
        initArrays[5] = arrayLength;

        /* Check Rearrange type */
        switch (arrayType)
        {
          case arrayRearrangeType.AUTO_DETECT:
          /* Check if init string contains an "{" >> Chooes struct init */
          if (variableInitValue.Contains("{"))
          {
            initArrays[8] = rearrangeStructArrayInitValues(variableInitValue);
          }
          else
          {
            initArrays[8] = rearrangeArrayInitValues(variableInitValue);
          }
          break;

          case arrayRearrangeType.STD_ARRAY:
          initArrays[8] = rearrangeArrayInitValues(variableInitValue);
          break;

          case arrayRearrangeType.STRUCT_ARRAY:
          initArrays[8] = rearrangeStructArrayInitValues(variableInitValue);
          break;

          default:
          throw new ArgumentOutOfRangeException("Array rearrange type is out of range.");
        }

        AddCFileObject(initArrays, variableDescription);
      }
    }

    /**
      * @brief      User function to add a single array with grouping but without description information
      * @param      string  array data type
      * @param      string  array name
      * @param      string  length of the array
      * @param      int     group of array
      * @param      string  array init value >> default none
      * @param      string  array access modifier >> default none 
      * @param      arrayRearrangeType >> Rearrange Type
      * @retval     None
      * @note       None
      */
    public void AddArray(string dataType, string variableName, string arrayLength, int groupIdx, string variableInitValue = "", string accessModifier = "", arrayRearrangeType arrayType = arrayRearrangeType.AUTO_DETECT)
    {
      /* Check if an init value is needed */
      if ("" == variableInitValue)
      {
        /* >>>>> Use no init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          arrays[0] = accessModifier + " ";
        }
        else
        {
          arrays[0] = accessModifier;
        }

        arrays[1] = dataType;
        arrays[3] = variableName;
        arrays[5] = arrayLength;
        AddCFileObject(arrays, groupIdx);
      }
      else
      {
        /* >>>>> Use init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          initArrays[0] = accessModifier + " ";
        }
        else
        {
          initArrays[0] = accessModifier;
        }

        initArrays[1] = dataType;
        initArrays[3] = variableName;
        initArrays[5] = arrayLength;

        /* Check Rearrange type */
        switch (arrayType)
        {
          case arrayRearrangeType.AUTO_DETECT:
          /* Check if init string contains an "{" >> Chooes struct init */
          if (variableInitValue.Contains("{"))
          {
            initArrays[8] = rearrangeStructArrayInitValues(variableInitValue);
          }
          else
          {
            initArrays[8] = rearrangeArrayInitValues(variableInitValue);
          }
          break;

          case arrayRearrangeType.STD_ARRAY:
          initArrays[8] = rearrangeArrayInitValues(variableInitValue);
          break;

          case arrayRearrangeType.STRUCT_ARRAY:
          initArrays[8] = rearrangeStructArrayInitValues(variableInitValue);
          break;

          default:
          throw new ArgumentOutOfRangeException("Array rearrange type is out of range.");
        }

        AddCFileObject(initArrays, groupIdx);
      }
    }

    /**
      * @brief      User function to add a single array with grouping and with description information
      * @param      string  array data type
      * @param      string  array name
      * @param      string  length of the array
      * @param      int     group of array
      * @param      DocumentationObject  array description
      * @param      string  array init value >> default none
      * @param      string  array access modifier >> default none
      * @param      arrayRearrangeType >> Rearrange Type
      * @retval     None
      * @note       None
      */
    public void AddArray(string dataType, string variableName, string arrayLength, int groupIdx, DocumentationObject variableDescription, string variableInitValue = "", string accessModifier = "", arrayRearrangeType arrayType = arrayRearrangeType.AUTO_DETECT)
    {
      /* Check if an init value is needed */
      if ("" == variableInitValue)
      {
        /* >>>>> Use no init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          arrays[0] = accessModifier + " ";
        }
        else
        {
          arrays[0] = accessModifier;
        }

        arrays[1] = dataType;
        arrays[3] = variableName;
        arrays[5] = arrayLength;
        AddCFileObject(arrays, groupIdx, variableDescription);
      }
      else
      {
        /* >>>>> Use init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          initArrays[0] = accessModifier + " ";
        }
        else
        {
          initArrays[0] = accessModifier;
        }

        initArrays[1] = dataType;
        initArrays[3] = variableName;
        initArrays[5] = arrayLength;

        /* Check Rearrange type */
        switch (arrayType)
        {
          case arrayRearrangeType.AUTO_DETECT:
          /* Check if init string contains an "{" >> Chooes struct init */
          if (variableInitValue.Contains("{"))
          {
            initArrays[8] = rearrangeStructArrayInitValues(variableInitValue);
          }
          else
          {
            initArrays[8] = rearrangeArrayInitValues(variableInitValue);
          }
          break;

          case arrayRearrangeType.STD_ARRAY:
          initArrays[8] = rearrangeArrayInitValues(variableInitValue);
          break;

          case arrayRearrangeType.STRUCT_ARRAY:
          initArrays[8] = rearrangeStructArrayInitValues(variableInitValue);
          break;

          default:
          throw new ArgumentOutOfRangeException("Array rearrange type is out of range.");
        }

        AddCFileObject(initArrays, groupIdx, variableDescription);
      }
    }

    /**
      * @brief      User function to add a single variable without any grouping or description information
      * @param      string  variable data type
      * @param      string  variable name
      * @param      string  variable init value >> default none
      * @param      string  variable access modifier >> default none 
      * @retval     None
      * @note       None
      */
    public void AddVariable(string dataType, string variableName, string variableInitValue = "", string accessModifier = "")
    {
      /* Check if an init value is needed */
      if("" == variableInitValue)
      {
        /* >>>>> Use no init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          variables[0] = accessModifier + " ";
        }
        else
        {
          variables[0] = accessModifier;
        }

        variables[1] = dataType;
        variables[3] = variableName;
        AddCFileObject(variables);
      }
      else
      {
        /* >>>>> Use init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          initVariables[0] = accessModifier + " ";
        }
        else
        {
          initVariables[0] = accessModifier;
        }

        initVariables[1] = dataType;
        initVariables[3] = variableName;
        initVariables[5] = variableInitValue;
        AddCFileObject(initVariables);
      }
		}


    /**
      * @brief      User function to add a single variable without any grouping but with description information
      * @param      string  variable data type
      * @param      string  variable name
      * @param      DocumentationObject  variable description
      * @param      string  variable init value >> default none
      * @param      string  variable access modifier >> default none 
      * @retval     None
      * @note       None
      */
    public void AddVariable(string dataType, string variableName, DocumentationObject variableDescription, string variableInitValue = "", string accessModifier = "")
    {
      /* Check if an init value is needed*/
      if ("" == variableInitValue)
      {
        /* >>>>> Use no init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          variables[0] = accessModifier + " ";
        }
        else
        {
          variables[0] = accessModifier;
        }

        variables[1] = dataType;
        variables[3] = variableName;
        AddCFileObject(variables, variableDescription);
      }
      else
      {
        /* >>>>> Use init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          initVariables[0] = accessModifier + " ";
        }
        else
        {
          initVariables[0] = accessModifier;
        }

        initVariables[1] = dataType;
        initVariables[3] = variableName;
        initVariables[5] = variableInitValue;
        AddCFileObject(initVariables, variableDescription);
      }
    }


    /**
      * @brief      User function to add a single variable with grouping but without description information
      * @param      string  variable data type
      * @param      string  variable name
      * @param      int     group of variable
      * @param      string  variable init value >> default none
      * @param      string  variable access modifier >> default none 
      * @retval     None
      * @note       None
      */
    public void AddVariable(string dataType, string variableName, int groupIdx, string variableInitValue = "", string accessModifier = "")
    {
      /* Check if an init value is needed */
      if ("" == variableInitValue)
      {
        /* >>>>> Use no init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          variables[0] = accessModifier + " ";
        }
        else
        {
          variables[0] = accessModifier;
        }

        variables[1] = dataType;
        variables[3] = variableName;
        AddCFileObject(variables, groupIdx);
      }
      else
      {
        /* >>>>> Use init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          initVariables[0] = accessModifier + " ";
        }
        else
        {
          initVariables[0] = accessModifier;
        }

        initVariables[1] = dataType;
        initVariables[3] = variableName;
        initVariables[5] = variableInitValue;
        AddCFileObject(initVariables, groupIdx);
      }
    }

    /**
      * @brief      User function to add a single variable with grouping and with description information
      * @param      string  variable data type
      * @param      string  variable name
      * @param      int     group of variable
      * @param      DocumentationObject  variable description
      * @param      string  variable init value >> default none
      * @param      string  variable access modifier >> default none 
      * @retval     None
      * @note       None
      */
    public void AddVariable(string dataType, string variableName, int groupIdx, DocumentationObject variableDescription, string variableInitValue = "", string accessModifier = "")
    {
      /* Check if an init value is needed */
      if ("" == variableInitValue)
      {
        /* >>>>> Use no init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          variables[0] = accessModifier + " ";
        }
        else
        {
          variables[0] = accessModifier;
        }

        variables[1] = dataType;
        variables[3] = variableName;
        AddCFileObject(variables, groupIdx, variableDescription);
      }
      else
      {
        /* >>>>> Use init value <<<<< */
        /* Check access modifier*/
        if ("" != accessModifier)
        {
          initVariables[0] = accessModifier + " ";
        }
        else
        {
          initVariables[0] = accessModifier;
        }

        initVariables[1] = dataType;
        initVariables[3] = variableName;
        initVariables[5] = variableInitValue;
        AddCFileObject(initVariables, groupIdx, variableDescription);
      }
    }

    /**
      * @brief      Internal helper function to aline the array int value to an maximum length
      * @param      string  Continues init values >> Splitt character ','
      * @retval     string  Rearranged init values
      * @note       None
      */
    private string rearrangeArrayInitValues(string initValues)
    {
      string[] tempStringArray;
      char[] splittChar = { ',' };
      string tempPartStringElement = "", tempStringElement = "", completeString = "";
      int maxSize = 0, cntNeededSpaces = 0;

      /* Splitt init value into single values to be rearranged from new*/
      tempStringElement = initValues.Replace(" ", string.Empty);
      tempStringArray = tempStringElement.Split(splittChar);

      /* Search for largest element */
      for(int idx=0; idx <  tempStringArray.Length; idx++)
      {
        if ((tempStringArray[idx].Length + 2) > maxSize)
        {
          maxSize = tempStringArray[idx].Length + 2;
        }
      }

      /* Rearrange Elements */
      for (int idx = 0; idx < tempStringArray.Length; idx++)
      {
        /* Calculate new element size */
        cntNeededSpaces = maxSize - (tempStringArray[idx].Length + 2);

        /* Check if element fits into current line */
        if( (tempPartStringElement.Length + tempStringArray[idx].Length + 2 + cntNeededSpaces) <= Convert.ToInt32(CFileResources.CSourceCntOfCharsPerLine) )
        {
          /* >>>>> Element fits into actual line <<<<< */
          /* Check if actual element ist last element */
          if (idx == ( tempStringArray.Length - 1 ))
          {
            tempPartStringElement += string.Concat(Enumerable.Repeat(' ', cntNeededSpaces)) + tempStringArray[idx];
          }
          else
          {
            tempPartStringElement += string.Concat(Enumerable.Repeat(' ', cntNeededSpaces)) + tempStringArray[idx] + ", ";
          }
        }
        else
        {
          /* >>>>> Element fits NOT into actual line <<<<< */
          /* Store current data and create new line */
          completeString += tempPartStringElement + "\n";

          /* Check if actual element ist last element */
          if (idx == ( tempStringArray.Length - 1 ))
          {
            tempPartStringElement = string.Concat(Enumerable.Repeat(' ', cntNeededSpaces)) + tempStringArray[idx];
          }
          else
          {
            tempPartStringElement = string.Concat(Enumerable.Repeat(' ', cntNeededSpaces)) + tempStringArray[idx] + ", ";
          }
        }

      }

      /* Store last elements */
      completeString += tempPartStringElement;

      return completeString;
    }

    /**
      * @brief      Internal helper function to aline the struct array int values
      * @param      string  Continues init values >> Splitt character ',' and '{'
      * @retval     string  Rearranged init values
      * @note       None
      */
    private string rearrangeStructArrayInitValues(string initValues)
    {
      string[] tempMainStringArray;
      char[] splittChar = { '{', '}' };
      int idx;
      string tempString = "";

      /* First splitt all struct data sets */
      tempMainStringArray = initValues.Split(splittChar);

      /* Rebuild Elements */
      for (idx = 0; idx < tempMainStringArray.Length; idx++)
      {
        if( !((tempMainStringArray[idx] == "") || ( tempMainStringArray[idx] == ",") || ( tempMainStringArray[idx] == " ,") || ( tempMainStringArray[idx] == ", " ) ) )
        {
          if("" == tempString)
          { tempString += "  { "; }
          else
          { tempString += ",\n  { "; }

          tempMainStringArray[idx] = tempMainStringArray[idx].Replace(" ", "");
          tempMainStringArray[idx] = tempMainStringArray[idx].Replace(",", ", ");
          tempString += tempMainStringArray[idx] + " }";
        }
      }

      return tempString;
    }

    /**
      * @brief      Interface function to start the string parser.
      * @param      string to be parsed
      * @retval     parse status >> true = parse sucessful
      * @detail     Parse the source string using all added elements.
      */
    public override bool ParseString(string str)
    {
      bool parseSuccessful;
      bool isStartString = false, groupCloserFound = false, groupOpenerFound = false, elementProcessed = false;
      string workingString = str, searchPattern;
      int processedChars, groupIndex, indexOfSearchPattern;
      GeneralStartGroupObject functionGroupOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject functionGroupCloser = new GeneralEndGroupObject();
      DoxygenElementDescription doxElementDescriptor = new DoxygenElementDescription();

      /* >>>>>>>>>> Start Parsing of the string <<<<<<<<<< */
      while (0 < workingString.Length)
      {
        /* Step 01: Check if elements are grouped */
        isStartString = functionGroupOpener.IsParseStringStartObject(workingString);
        if (true == isStartString)
        {
          groupOpenerFound = true;
          processedChars = functionGroupOpener.ParseString(workingString);

          /* Remove grouping information from working string */
          workingString = workingString.Remove(0, processedChars);
        }

        /* >>>> Process grouped elements <<<< */
        if (true == groupOpenerFound)
        {
          elementProcessed = false;

          /* Create group closer */
          functionGroupCloser.AddOsarStopMemMapDefine(functionGroupOpener.GetOsarModuleName(), functionGroupOpener.GetOsarMemMapType());
          groupIndex = AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);

          while (false == groupCloserFound)
          {
            /* Step 02: Check for documentation */
            doxElementDescriptor = new DoxygenElementDescription();
            isStartString = doxElementDescriptor.IsParseStringStartObject(workingString);
            if (true == isStartString)
            {
              elementProcessed = true;
              processedChars = doxElementDescriptor.ParseString(workingString);

              /* Remove documentation information from working string */
              workingString = workingString.Remove(0, processedChars);
            }


            /* Step 03: Parse variable elements */
            isStartString = IsParseStringStartVariableObject(workingString);
            if (true == isStartString)
            {
              /* Check if doxygen comment is available */
              if (true == elementProcessed)
              {
                processedChars = ParseVariableString(workingString, groupIndex, doxElementDescriptor);
              }
              else
              {
                processedChars = ParseVariableString(workingString, groupIndex, null);
              }


              /* Remove element information from working string */
              processedChars++; // Remove also the additional '\n' after each variable
              workingString = workingString.Remove(0, processedChars);
              elementProcessed = true;
            }



            /* Check of one element has been processed */
            if (false == elementProcessed)
            {
              groupCloserFound = true;
            }
            else
            {
              elementProcessed = false;
            }
          }

          /* Remove Group Closer from working string (two lines)*/
          searchPattern = "\n";
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);

          /* Reset Grouping Flags */
          groupOpenerFound = false;
          groupCloserFound = false;
        }
        else
        {
          /* Process Include strings without grouping information */
          elementProcessed = true;
          while (true == elementProcessed)
          {
            elementProcessed = false;

            /* Step 02: Check for documentation */
            doxElementDescriptor = new DoxygenElementDescription();
            isStartString = doxElementDescriptor.IsParseStringStartObject(workingString);
            if (true == isStartString)
            {
              elementProcessed = true;
              processedChars = doxElementDescriptor.ParseString(workingString);

              /* Remove documentation information from working string */
              workingString = workingString.Remove(0, processedChars);
            }

            /* Step 03: Parse variable elements */
            isStartString = IsParseStringStartVariableObject(workingString);
            if (true == isStartString)
            {
              /* Check if doxygen comment is available */
              if(true == elementProcessed)
              {
                processedChars = ParseVariableString(workingString, -1, doxElementDescriptor);
              }
              else
              {
                processedChars = ParseVariableString(workingString, -1, null);
              }


              /* Remove element information from working string */
              processedChars++; // Remove also the additional '\n' after each variable
              workingString = workingString.Remove(0, processedChars);
              elementProcessed = true;
            }

            /* Check of one element has been processed */
            if (true == elementProcessed)
            {
              elementProcessed = true;
            }
            else
            {
              elementProcessed = false;
            }
          }
        }
      }

      /* Check if parsing was successful */
      if (0 == workingString.Length)
      { parseSuccessful = true; }
      else
      { parseSuccessful = false; }

      return parseSuccessful;
    }

    /**
      * @brief      Interface function to check if selected element is start element of the string
      * @param      string to be checked
      * @retval     bool >> true = element is start element of string
      * @note       A direct detection if the object ist the actual start element is not possible. So an
      *             indirect detection must be used.
      */
    public bool IsParseStringStartVariableObject(string str)
    {
      bool retVal = true;
      string searchPattern;
      int indexOfSearchPattern;

      /* >>> Check for an include <<< */
      searchPattern = "#include \"";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);

      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = false;
      }

      /* >>> Check for an define <<< */
      searchPattern = "#define ";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);

      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = false;
      }

      /* >>> Check for an Comment start <<< */
      searchPattern = "/*";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);

      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = false;
      }

      /* >>> Check for an Comment start <<< */
      searchPattern = "//";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);

      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = false;
      }

      return retVal;
    }

    /**
      * @brief      Interface function to start the string parser for an variable string.
      * @param      string to be parsed
      * @param      int group id
      * @param      DocumentationObject variableDescription
      * @retval     Count of chars which has been parsed
      */
    private int ParseVariableString(string str, int groupId, DocumentationObject variableDescription)
    {
      bool exist;
      string searchPattern, workingString;
      int indexOfSearchPattern, processedChars = 0;

      /* Check Type of variable data to be parsed */
      searchPattern = ";\n";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      workingString = str.Substring(0, indexOfSearchPattern);
      processedChars += searchPattern.Length;

      /* Check type of variable */
      searchPattern = "[";
      exist = workingString.Contains(searchPattern);

      if(true == exist)
      {
        processedChars += ParseArrayString(workingString, groupId, variableDescription);
      }
      else
      {
        processedChars += ParseStdVariableString(workingString, groupId, variableDescription);
      }

      return processedChars;
    }

    /**
      * @brief      Interface function to start the string parser for an standard variable string.
      * @param      string to be parsed
      * @param      int group id
      * @param      DocumentationObject variableDescription
      * @retval     Count of chars which has been parsed
      */
    private int ParseStdVariableString(string str, int groupId, DocumentationObject variableDescription)
    {
      int idx = 0;
      char[] splitPattern = { ' ' };
      string[] stringElements;
      string accessModifiers = "";
      string dataType = "", name = "", content = "";

      /* Split elements */
      stringElements = str.Split(splitPattern);

      /* Parse string elements */
      /* Process all access modifieres */
      while(true == IsAccessModifier(stringElements[idx]))
      {
        if ("" == accessModifiers)
        { accessModifiers += stringElements[idx]; }
        else
        { accessModifiers += " " + stringElements[idx]; }
        idx++;
      }

      /* Process variable type */
      dataType = stringElements[idx];
      idx++;

      /* Process variable name */
      name = stringElements[idx];

      /* Process variable content */
      /* Check if content is available */
      if(stringElements.Count() >= ( idx + 2 ))
      {
        idx++; // '='
        idx++; // content
        content = stringElements[idx];
      }
      else
      {
        content = "";
      }

      /* Add variable information */
      if(-1 == groupId)
      {
        if(null == variableDescription)
        {
          AddVariable(dataType, name, content, accessModifiers);
        }
        else
        {
          AddVariable(dataType, name, variableDescription, content, accessModifiers);
        }
      }
      else
      {
        if (null == variableDescription)
        {
          AddVariable(dataType, name, groupId,content, accessModifiers);
        }
        else
        {
          AddVariable(dataType, name, groupId, variableDescription, content, accessModifiers);
        }
      }

      return str.Length;
    }

    /**
      * @brief      Interface function to start the string parser for an array variable string.
      * @param      string to be parsed
      * @param      int group id
      * @param      DocumentationObject variableDescription
      * @retval     Count of chars which has been parsed
      */
    private int ParseArrayString(string str, int groupId, DocumentationObject variableDescription)
    {
      int idx = 0, idx2;
      char[] splitPattern = { ' ' };
      string[] stringElements, stringElements2;
      string accessModifiers = "";
      string dataType = "", name = "", content = "", length = "";


      /* Splitt elements */
      stringElements = str.Split(splitPattern);

      /* Parse string elements */
      /* Process all access modifieres */
      while (true == IsAccessModifier(stringElements[idx]))
      {
        if ("" == accessModifiers)
        { accessModifiers += stringElements[idx]; }
        else
        { accessModifiers += " " + stringElements[idx]; }
        idx++;
      }

      /* Process variable type */
      dataType = stringElements[idx];
      idx++;

      /* Process variable name */
      stringElements2 = stringElements[idx].Split('[');
      name = stringElements2[0];

      /* Process array length */
      length = stringElements2[1];
      length = length.Remove(length.Length -1 , 1); //']'

      /* Process variable content */
      /* Check if content is available */
      if (stringElements.Count() >= ( idx + 2 ))
      {
        idx++; // '='
        idx++; // '{'
        idx++; // content

        for( idx2 = idx; idx2 < (stringElements.Count() -1) ; idx2++)
        {
          content += stringElements[idx2] + " ";
        }

        /* Add last array element */
        stringElements2 = stringElements[stringElements.Count() - 1].Split('\n');
        content += stringElements2[0];

        /* Remove \n */
        content = content.Replace("\n", "");
        /* Remove multiple spaces */
        for(idx2 = 0; idx2 < 10; idx2++)
        {
          content = content.Replace("  ", " ");
        }
        
      }
      else
      {
        content = "";
      }



      /* Add variable information */
      if (-1 == groupId)
      {
        if (null == variableDescription)
        {
          AddVariable(dataType, name, content, accessModifiers);
        }
        else
        {
          AddVariable(dataType, name, variableDescription, content, accessModifiers);
        }
      }
      else
      {
        if (null == variableDescription)
        {
          AddVariable(dataType, name, groupId, content, accessModifiers);
        }
        else
        {
          AddVariable(dataType, name, groupId, variableDescription, content, accessModifiers);
        }
      }

      return str.Length;
    }

    /**
      * @brief      Helper functiont o detect if element is an access modifier
      * @param      string to be checked
      * @retval     true > String is an access modifier
      * @note       Access modifier "extern" | "volatile" | "static" | "const"
      */
    private bool IsAccessModifier(string str)
    {
      bool retVal = false;

      if("extern" == str)
      { retVal = true; }

      if ("volatile" == str)
      { retVal = true; }

      if ("static" == str)
      { retVal = true; }

      if ("const" == str)
      { retVal = true; }

      return retVal;
    }

  }//end CFileVariableObjects

}//end namespace CFileObjects