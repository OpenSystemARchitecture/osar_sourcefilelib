///////////////////////////////////////////////////////////
//  objectsWithGroups.cs
//  Implementation of the Class singleObject
//  Generated by Enterprise Architect
//  Created on:      03-Feb-2018 09:27:45
//  Original author: SRein
///////////////////////////////////////////////////////////
/* Used Namespaces -----------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using OsarSourceFileLib.DocumentationObjects;

/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.CFile.CFileObjects
{
  /**
  * @brief      Struct for generic objects with grouping informations
  * @details    Struct elements:
  *             >> public OsarSourceFileLib.DocumentationObjects.DocumentationObject objectEndGroup
  *             >> public OsarSourceFileLib.DocumentationObjects.DocumentationObject objectStartGroup
  *             >> public List<singleObject> objects
  */
  public struct objectsWithGroups
  {

    //public OsarSourceFileLib.DocumentationObjects.DocumentationObject objectEndGroup;
    public string objectEndGroup;

    public List<singleObject> objects;

    //public OsarSourceFileLib.DocumentationObjects.DocumentationObject objectStartGroup;
    public string objectStartGroup;

  }//end objectsWithGroups

}//end namespace CFileObjects

