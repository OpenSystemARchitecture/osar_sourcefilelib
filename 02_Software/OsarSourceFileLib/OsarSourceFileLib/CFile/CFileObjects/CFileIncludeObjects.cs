﻿/*****************************************************************************************************************************
 * @file        CFileIncludeObjects.cs                                                                                       *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        15.02.2018 11:08:24                                                                                          *
 * @brief       CFileObject >> Include object class.                                                                         *
 * @details     This class is used to implement an include class.                                                            *
*****************************************************************************************************************************/
/* Used Namespaces -----------------------------------------------------------*/
using OsarSourceFileLib.DocumentationObjects;
using OsarSourceFileLib.Exceptions.CFileObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarSourceFileLib.DocumentationObjects.General;
using OsarSourceFileLib.DocumentationObjects.Doxygen;

/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.CFile.CFileObjects
{
  /**
  * @brief      CFile Include-Object
  * @note       This class is used to realize C-Include functionalities of an CFile Object 
  * @details    Base class:
  *             >> CFileBaseObject
  */
  public class CFileIncludeObjects : CFileBaseObject
  {
    private string[] includes = { "#include \"", "" ,"\""};

    /**
      * @brief      Constructor
      */
    public CFileIncludeObjects() : base()
    {
    }

    /**
      * @brief      Destructor
      */
    ~CFileIncludeObjects()
    {
    }

    /**
      * @brief      User function to add a single include without any grouping or description information
      * @param      string  include file name
      * @retval     None
      * @note       None
      */
    public void AddIncludeFile(string includeFileName)
    {
      includes[1] = includeFileName;
      AddCFileObject(includes);
    }

    /**
      * @brief      User function to add a single include without any grouping but with description information
      * @param      string  include file name
      * @param      DocumentationObject with include description
      * @retval     None
      * @note       None
      */
    public void AddIncludeFile(string includeFileName, DocumentationObject includeDescription)
    {
      includes[1] = includeFileName;
      AddCFileObject(includes, includeDescription);
    }

    /**
      * @brief      User function to add an include within an defined group but without description information
      * @param      string  include file name
      * @param      int Index of the group which shall be used.
      * @retval     None
      * @note       An group as to be created first:
      *             Exceptions:
      *               >> OsarFileLibException_GroupNotAvailable
      */
    public void AddIncludeFile(string includeFileName, int groupIdx)
    {
      includes[1] = includeFileName;

      /* Check if group is available */
      if (( objectListWithGroups.Count - 1 ) < groupIdx)
      {
        throw new OsarFileLibException_GroupNotAvailable(OsarFileLibErrorStrings.SystemNameLibName + OsarFileLibErrorStrings.Error0001_FilePathEmpty);
      }

      AddCFileObject(includes, groupIdx);
    }

    /**
      * @brief      User function to add an include within an defined group and with description information
      * @param      string  include file name
      * @param      int Index of the group which shall be used.
      * @param      DocumentationObject with include description
      * @retval     None
      * @note       An group as to be created first:
      *             Exceptions:
      *               >> OsarFileLibException_GroupNotAvailable
      */
    public void AddIncludeFile(string includeFileName, int groupIdx, DocumentationObject includeDescription)
    {
      includes[1] = includeFileName;
      AddCFileObject(includes, groupIdx, includeDescription);
    }

    /**
      * @brief      Interface function to start the string parser.
      * @param      string to be parsed
      * @retval     parse status >> true = parse sucessful
      * @detail     Parse the source string using all added elements.
      */
    public override bool ParseString(string str)
    {
      bool parseSuccessful;
      bool isStartString = false, groupCloserFound = false, groupOpenerFound = false, elementProcessed = false;
      string workingString = str, includeString, searchPattern;
      int processedChars, groupIndex, indexOfSearchPattern;
      GeneralStartGroupObject functionGroupOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject functionGroupCloser = new GeneralEndGroupObject();
      DoxygenElementDescription doxElementDescriptor = new DoxygenElementDescription();

      List<DoxygenElementDescription> listGroupDoxygen = new List<DoxygenElementDescription>();
      List<string> listGroupIncludes = new List<string>();

      while (0 < workingString.Length)
      {
        /* Step 01: Check if elements are grouped */
        isStartString = functionGroupOpener.IsParseStringStartObject(workingString);
        if(true == isStartString)
        {
          groupOpenerFound = true;
          processedChars = functionGroupOpener.ParseString(workingString);

          /* Remove grouping information from working string */
          workingString = workingString.Remove(0, processedChars);
        }

        /* >>>> Process grouped elements <<<< */
        if(true == groupOpenerFound)
        {
          elementProcessed = false;
          while (false == groupCloserFound)
          {
            /* Step 02: Check for documentation */
            doxElementDescriptor = new DoxygenElementDescription();
            isStartString = doxElementDescriptor.IsParseStringStartObject(workingString);
            if (true == isStartString)
            {
              elementProcessed = true;
              processedChars = doxElementDescriptor.ParseString(workingString);
              listGroupDoxygen.Add(doxElementDescriptor);

              /* Remove documentation information from working string */
              workingString = workingString.Remove(0, processedChars);

            }
            else
            {
              listGroupDoxygen.Add(null);
            }

            /* Step 03: Parse include elements */
            isStartString = IsParseStringStartIncludeObject(workingString);
            if (true == isStartString)
            {
              elementProcessed = true;
              processedChars = ParseIncludeString(workingString, out includeString);

              /* Remove element information from working string */
              workingString = workingString.Remove(0, processedChars);
              listGroupIncludes.Add(includeString);
            }

            /* Step 04: Check for group closer elements */
            isStartString = functionGroupCloser.IsParseStringStartObject(workingString);
            if(true == isStartString)
            {
              processedChars = functionGroupCloser.ParseString(workingString);
              workingString = workingString.Remove(0, processedChars);
              groupCloserFound = true;
            }
          }

          /* Add include file Objects with grouping information */
          /* Consturct group end element */
          functionGroupCloser.AddOsarStopMemMapDefine(functionGroupOpener.GetOsarModuleName(), functionGroupOpener.GetOsarMemMapType());
          groupIndex = AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);

          for (int idx = 0; idx < listGroupIncludes.Count(); idx++)
          {
            if(null == listGroupDoxygen[idx])
            {
              AddIncludeFile(listGroupIncludes[idx], groupIndex);
            }
            else
            {
              AddIncludeFile(listGroupIncludes[idx], groupIndex, listGroupDoxygen[idx]);
            }
          }

          /* Remove Group Closer from working string (two lines)*/
          searchPattern = "\n";
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);

          /* Reset Grouping Flags */
          groupOpenerFound = false;
          groupCloserFound = false;

        }
        else
        {
          /* Process Include strings without grouping information */
          elementProcessed = true;
          while (true == elementProcessed)
          {
            elementProcessed = false;

            /* Step 02: Check for documentation */
            doxElementDescriptor = new DoxygenElementDescription();
            isStartString = doxElementDescriptor.IsParseStringStartObject(workingString);
            if (true == isStartString)
            {
              elementProcessed = true;
              processedChars = doxElementDescriptor.ParseString(workingString);

              /* Remove documentation information from working string */
              workingString = workingString.Remove(0, processedChars);
            }

            /* Step 03: Parse include elements */
            isStartString = IsParseStringStartIncludeObject(workingString);
            if (true == isStartString)
            {
              processedChars = ParseIncludeString(workingString, out includeString);

              /* Remove element information from working string */
              workingString = workingString.Remove(0, processedChars);

              if(true == elementProcessed)
              {
                AddIncludeFile(includeString, doxElementDescriptor);
              }
              else
              {
                AddIncludeFile(includeString);
              }

              elementProcessed = true;
            }

            /* Check of one element has been processed */
            if (true == elementProcessed)
            {
              elementProcessed = true;
            }
            else
            {
              elementProcessed = false;
            }
          }
        }
      }

      /* Check if parsing was successful */
      if(0 == workingString.Length)
      { parseSuccessful = true; }
      else
      { parseSuccessful = false; }

      return parseSuccessful;
    }

    /**
      * @brief      Interface function to check if selected element is start element of the string
      * @param      string to be checked
      * @retval     bool >> true = element is start element of string
      */
    public bool IsParseStringStartIncludeObject(string str)
    {
      bool retVal;
      string searchPattern;
      int indexOfSearchPattern;

      searchPattern = "#include \"";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = true;
      }
      else
      {
        retVal = false;
      }

      return retVal;
    }

    /**
      * @brief      Interface function to start the string parser for an include string.
      * @param      string to be parsed
      * @param      string parsed include file
      * @retval     count of chars which has been parsed
      */
    private int ParseIncludeString(string str, out string includeFile)
    {
      string searchPattern, workingString;
      int indexOfSearchPattern, processedChars = 0;
      includeFile = "";

      /* Step 1 > Search for start pattern and cut it out */
      workingString = str;
      searchPattern = "#include \"";
      indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);

      if (0 == indexOfSearchPattern)
      {
        /* Remove all behinde the end string*/
        workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
      }

      foreach (char c in workingString)
      {
        if('\"' == c)
        { break; }
        else
        {
          includeFile += c;
        }
      }

      /* Calculate processed elements */
      searchPattern = "\n";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      processedChars = indexOfSearchPattern + searchPattern.Length;

      return processedChars;
    }

  }
}
