﻿/*****************************************************************************************************************************
 * @file        CFileBaseObject.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        15.02.2018 11:08:24                                                                                          *
 * @brief       CFileObject >> Base object class.                                                                            *
 * @details     This class is used to implement an abstract base class to implement an generic object handling               *
*****************************************************************************************************************************/
/* Used Namespaces -----------------------------------------------------------*/
using OsarSourceFileLib.DocumentationObjects;
using OsarSourceFileLib.Exceptions.CFileObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.CFile.CFileObjects
{
  /**
  * @brief      CFile Base-Object
  * @note       This class is used to realize basic functionalities of an CFile Object 
  * @details    Used Interfaces:
  *             > CFileObjectGenericInterface
  */
  public class CFileBaseObject : CFileObjectGenericInterface
  {
    protected List<objectsWithGroups> objectListWithGroups;
    protected List<singleObject> singleObjectList;

    /**
      * @brief      Constructor
      */
    protected CFileBaseObject()
    {
      objectListWithGroups = new List<objectsWithGroups>();
      singleObjectList = new List<singleObject>();
    }

    /**
      * @brief      Destructor
      */
    ~CFileBaseObject()
    {

    }

    /**
      * @brief      Generic function to add an single define without any grouping or description information
      * @param      string  define name and content
      * @retval     None
      * @note       None
      */
    protected void AddCFileObject(string[] cFileObject)
    {
      singleObject newSingleObject;
      newSingleObject.objectString = new string[cFileObject.Length];
      cFileObject.CopyTo(newSingleObject.objectString, 0);
      newSingleObject.objectDesription = null;
      singleObjectList.Add(newSingleObject);
    }

    /**
      * @brief      Generic function to add an single define without any grouping but with description information
      * @param      string  define name and content
      * @param      DocumentationObject with define description
      * @retval     None
      * @note       None
      */
    protected void AddCFileObject(string[] cFileObject, DocumentationObject objectDescription)
    {
      singleObject newSingleObject;
      newSingleObject.objectString = new string[cFileObject.Length];
      cFileObject.CopyTo(newSingleObject.objectString, 0);
      newSingleObject.objectDesription = objectDescription.GenerateString();
      singleObjectList.Add(newSingleObject);
    }

    /**
      * @brief      Generic function to add an define within an defined group but without description information
      * @param      string  define name and content
      * @param      int Index of the group which shall be used.
      * @retval     None
      * @note       An group as to be created first:
      *             Exceptions:
      *               >> OsarFileLibException_GroupNotAvailable
      */
    protected void AddCFileObject(string[] cFileObject, int groupIdx)
    {
      /* Check if group is available */
      if (( objectListWithGroups.Count - 1 ) < groupIdx)
      {
        throw new OsarFileLibException_GroupNotAvailable(OsarFileLibErrorStrings.SystemNameLibName + OsarFileLibErrorStrings.Error0001_FilePathEmpty);
      }

      singleObject newSingleObject;
      newSingleObject.objectString = new string[cFileObject.Length];
      cFileObject.CopyTo(newSingleObject.objectString, 0);
      newSingleObject.objectDesription = null;

      objectListWithGroups[groupIdx].objects.Add(newSingleObject);
    }

    /**
    * @brief      Generic function to add an define within an defined group and with description information
    * @param      string  define name and content
    * @param      int Index of the group which shall be used.
    * @param      DocumentationObject with define description
    * @retval     None
    * @note       An group as to be created first:
    *             Exceptions:
    *               >> OsarFileLibException_GroupNotAvailable
    */
    protected void AddCFileObject(string[] cFileObject, int groupIdx, DocumentationObject objectDescription)
    {
      /* Check if group is available */
      if (( objectListWithGroups.Count - 1 ) < groupIdx)
      {
        throw new OsarFileLibException_GroupNotAvailable(OsarFileLibErrorStrings.SystemNameLibName + OsarFileLibErrorStrings.Error0001_FilePathEmpty);
      }

      singleObject newSingleObject;
      newSingleObject.objectString = new string[cFileObject.Length];
      cFileObject.CopyTo(newSingleObject.objectString,0); 
      newSingleObject.objectDesription = objectDescription.GenerateString();

      objectListWithGroups[groupIdx].objects.Add(newSingleObject);
    }

    /**
      * @brief      Generic function to add an new define group
      * @param      DocumentationObject   Star group object 
      * @param      DocumentationObject   End group object
      * @retval     None
      * @note       None
      */
    public int AddCFileObjectGroup(DocumentationObject cFileObjectStartGroup, DocumentationObject cFileObjectEndGroupGroup)
    {
      objectsWithGroups newObjectsWithGroups;
      newObjectsWithGroups.objectStartGroup = cFileObjectStartGroup.GenerateString();
      newObjectsWithGroups.objectEndGroup = cFileObjectEndGroupGroup.GenerateString();
      newObjectsWithGroups.objects = new List<singleObject>();

      objectListWithGroups.Add(newObjectsWithGroups);

      return objectListWithGroups.Count - 1;
    }


    /**
      * @brief      User function to get the single Objects
      * @param      None
      * @retval     List<singleObject>  Single Object List
      * @note       None
      */
    public List<singleObject> GetSingleObjects()
    {
      return singleObjectList;
    }

    /**
      * @brief      User function to get the grouped objects
      * @param      None
      * @retval     List<objectsWithGroups>  Grouped Object List
      * @note       None
      */
    public List<objectsWithGroups> GetGroupedObjects()
    {
      return objectListWithGroups;
    }

    /**
      * @brief      Used to generate from object the effective ASCII string.
      * @param      None
      * @retval     String >> Generated include String
      * @note       None
      */
    public string GenerateString()
    {
      string completeStringElement = "";

      /* Step 01: Generate Objects without an group */
      for (int idx = 0; idx < singleObjectList.Count; idx++)
      {

        if (null != singleObjectList[idx].objectDesription)
        {
          completeStringElement += singleObjectList[idx].objectDesription;
        }

        for(int idx2 = 0; idx2< singleObjectList[idx].objectString.Length; idx2++)
        {
          completeStringElement += singleObjectList[idx].objectString[idx2];
        }
        completeStringElement += "\n";
      }

      /* Step 02: Generate Objects with an group */
      for (int idx = 0; idx < objectListWithGroups.Count; idx++)
      {
        completeStringElement += objectListWithGroups[idx].objectStartGroup;

        for (int idx2 = 0; idx2 < objectListWithGroups[idx].objects.Count; idx2++)
        {
          if (null != objectListWithGroups[idx].objects[idx2].objectDesription)
          {
            completeStringElement += objectListWithGroups[idx].objects[idx2].objectDesription;
          }

          for (int idx3 = 0; idx3 < objectListWithGroups[idx].objects[idx2].objectString.Length; idx3++)
          {
            completeStringElement += objectListWithGroups[idx].objects[idx2].objectString[idx3];
          }
          completeStringElement += "\n";
        }



        completeStringElement += objectListWithGroups[idx].objectEndGroup;
      }


      return completeStringElement;
    }

    /**
      * @brief      Interface function to start the string parser.
      * @param      string to be parsed
      * @retval     parse status >> true = parse sucessful
      * @detail     Parse the source string using all added elements.
      */
    public virtual bool ParseString(string str)
    {
      /* Step 01: Parse Objects without an group */

      /* Step 02: Parse Objects with an group */
      throw new NotImplementedException();
    }
  }
}
