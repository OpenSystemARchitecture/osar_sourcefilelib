/*****************************************************************************************************************************
 * @file        CFileFunctionObjects.cs                                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        15.02.2018 11:08:24                                                                                          *
 * @brief       CFileObject >> Function object class.                                                                        *
 * @details     This class is used to implement an function class.                                                           *
*****************************************************************************************************************************/
/* Used Namespaces -----------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using OsarSourceFileLib.DocumentationObjects.General;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.DocumentationObjects;
using OsarSourceFileLib.CFile.CFileObjects;
/* Current Namespaces --------------------------------------------------------*/
namespace OsarSourceFileLib.CFile.CFileObjects {
  /**
    * @brief      Gerneric CFile Object for functions
    * @details    Used Interfaces:
    *             >> CFileObjectFunctionInterface
    *             Base class:
    *             >> CFileBaseObject
    */
  public class CFileFunctionObjects : CFileBaseObject, CFileObjectFunctionInterface
  {
    private string[] functions = { "", "", "", "\n{\n", "" ,"\n}", "\n"};   //0 == Return type
                                                                        //1 == Function name
                                                                        //2 == Arguments
                                                                        //3 == Start function
                                                                        //4 == Function content
                                                                        //5 == End function
    /**
      * @brief      Constructor
      */
    public CFileFunctionObjects() : base()
    {
	}

    /**
      * @brief      Destructor
      */
    ~CFileFunctionObjects()
    {
	}

    /**
      * @brief      User function to add an single function without any grouping or description information
      * @param      string                    function return type
      * @param      string                    function name
      * @param      string                    function arguments
      * @param      string                    function content >> Default Empty
      * @retval     None
      * @note       None
      */
    public void AddFunction(string returnType, string functionName, string functionParameter, string functionContent = "")
    {
      createFunctionInformations(returnType, functionName, functionParameter, functionContent);
      AddCFileObject(functions);
    }

    /**
      * @brief      User function to add an single function without any grouping but with description information
      * @param      string                    function return type
      * @param      string                    function name
      * @param      string                    function arguments
      * @param      DocumentationObject       function description 
      * @param      string                    function content >> Default empty
      * @retval     None
      * @note       None
      */
    public void AddFunction(string returnType, string functionName, string functionParameter, DocumentationObject functionDescription, string functionContent = "")
    {
      createFunctionInformations(returnType, functionName, functionParameter, functionContent);
      AddCFileObject(functions, functionDescription);
    }

    /**
      * @brief      User function to add an function within an defined group but without description information
      * @param      string                    function return type
      * @param      string                    function name
      * @param      string                    function arguments
      * @param      int                       group index 
      * @param      string                    function content >> Default empty
      * @retval     None
      * @note       An group as to be created first:
      *             Exceptions:
      *               >> OsarFileLibException_GroupNotAvailable
      */
    public void AddFunction(string returnType, string functionName, string functionParameter, int groupIdx, string functionContent = "")
    {
      createFunctionInformations(returnType, functionName, functionParameter, functionContent);
      AddCFileObject(functions, groupIdx);
    }

    /**
      * @brief      User function to add an function within an defined group and with description information
      * @param      string                    function return type
      * @param      string                    function name
      * @param      string                    function arguments
      * @param      int                       group index 
      * @param      DocumentationObject       function description 
      * @param      string                    function content >> Default empty
      * @retval     None
      * @note       An group as to be created first:
      *             Exceptions:
      *               >> OsarFileLibException_GroupNotAvailable
      */
    public void AddFunction(string returnType, string functionName, string functionParameter, int groupIdx, DocumentationObject functionDescription, string functionContent = "")
    {
      createFunctionInformations(returnType, functionName, functionParameter, functionContent);
      AddCFileObject(functions, groupIdx, functionDescription);
    }

    /**
      * @brief      User function to add an single function without any grouping but with description information
      * @retval     string  function prototypes
      */
    public string GenerateFunctionPrototypeString()
    {
      string completeStringElement = "";

      /* Step 01: Generate Objected without an group */
      for (int idx = 0; idx < singleObjectList.Count; idx++)
      {
        if (null != singleObjectList[idx].objectDesription)
        {
          completeStringElement += singleObjectList[idx].objectDesription;
        }

        completeStringElement += singleObjectList[idx].objectString[0] + singleObjectList[idx].objectString[1] + singleObjectList[idx].objectString[2] + ";";
        completeStringElement += "\n\n";
      }

      /* Step 02: Generate Objected with an group */
      for (int idx = 0; idx < objectListWithGroups.Count; idx++)
      {
        completeStringElement += objectListWithGroups[idx].objectStartGroup;

        for (int idx2 = 0; idx2 < objectListWithGroups[idx].objects.Count; idx2++)
        {
          if (null != objectListWithGroups[idx].objects[idx2].objectDesription)
          {
            completeStringElement += objectListWithGroups[idx].objects[idx2].objectDesription;
          }

          completeStringElement += objectListWithGroups[idx].objects[idx2].objectString[0] + objectListWithGroups[idx].objects[idx2].objectString[1] + objectListWithGroups[idx].objects[idx2].objectString[2] + ";";
          completeStringElement += "\n\n";
        }

        completeStringElement += objectListWithGroups[idx].objectEndGroup;
      }

      return completeStringElement;
 		}

    /**
      * @brief      Internal helper function to get an equal spaces between definition and value
      * @param      string                    function return type
      * @param      string                    function name
      * @param      string                    function arguments
      * @param      string                    function content
      * @retval     None
      * @note       Function will fill automatical the object private "functions" variable
      */
    private void createFunctionInformations(string returnType, string functionName, string functionParameter, string functionContent, bool isPrivate = false)
    {
      string tempString = "";
      functions[0] = returnType + " ";
      functions[1] = functionName;

      /* Check function parameters */
      if ("" == functionParameter)
      {
        functions[2] = "( void )";
      }
      else
      {
        tempString += "( ";
        tempString += functionParameter;
        tempString += " )";
        functions[2] = tempString;
      }

      functions[4] = functionContent;
    }

    /**
      * @brief      Interface function to start the string parser.
      * @param      string to be parsed
      * @retval     parse status >> true = parse sucessful
      * @detail     Parse the source string using all added elements.
      */
    public override bool ParseString(string str)
    {
      bool parseSuccessful;
      bool isStartString = false, groupCloserFound = false, groupOpenerFound = false, elementProcessed = false;
      string workingString = str, searchPattern;
      int processedChars, groupIndex, indexOfSearchPattern;
      GeneralStartGroupObject functionGroupOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject functionGroupCloser = new GeneralEndGroupObject();
      DoxygenElementDescription doxElementDescriptor = new DoxygenElementDescription();

      /* >>>>>>>>>> Start Parsing of the string <<<<<<<<<< */
      while (0 < workingString.Length)
      {
        /* Step 01: Check if elements are grouped */
        isStartString = functionGroupOpener.IsParseStringStartObject(workingString);
        if (true == isStartString)
        {
          groupOpenerFound = true;
          processedChars = functionGroupOpener.ParseString(workingString);

          /* Remove grouping information from working string */
          workingString = workingString.Remove(0, processedChars);
        }

        /* >>>> Process grouped elements <<<< */
        if (true == groupOpenerFound)
        {
          elementProcessed = false;

          /* Create group closer */
          functionGroupCloser.AddOsarStopMemMapDefine(functionGroupOpener.GetOsarModuleName(), functionGroupOpener.GetOsarMemMapType());
          groupIndex = AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);

          while (false == groupCloserFound)
          {
            /* Step 02: Check for documentation */
            doxElementDescriptor = new DoxygenElementDescription();
            isStartString = doxElementDescriptor.IsParseStringStartObject(workingString);
            if (true == isStartString)
            {
              elementProcessed = true;
              processedChars = doxElementDescriptor.ParseString(workingString);

              /* Remove documentation information from working string */
              workingString = workingString.Remove(0, processedChars);
            }


            /* Step 03: Parse function elements */
            isStartString = IsParseStringStartFunctionObject(workingString);
            if (true == isStartString)
            {
              /* Check if doxygen comment is available */
              if (true == elementProcessed)
              {
                processedChars = ParseFunctionString(workingString, groupIndex, doxElementDescriptor);
              }
              else
              {
                processedChars = ParseFunctionString(workingString, groupIndex, null);
              }


              /* Remove element information from working string */
              processedChars++; // Remove also the additional '\n' after each variable
              workingString = workingString.Remove(0, processedChars);
              elementProcessed = true;
            }



            /* Check of one element has been processed */
            if (false == elementProcessed)
            {
              groupCloserFound = true;
            }
            else
            {
              elementProcessed = false;
            }
          }

          /* Remove Group Closer from working string (two lines)*/
          searchPattern = "\n";
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);
          indexOfSearchPattern = workingString.IndexOf(searchPattern, 0, workingString.Length, StringComparison.Ordinal);
          workingString = workingString.Remove(0, indexOfSearchPattern + searchPattern.Length);

          /* Reset Grouping Flags */
          groupOpenerFound = false;
          groupCloserFound = false;
        }
        else
        {
          /* Process Include strings without grouping information */
          elementProcessed = true;
          while (true == elementProcessed)
          {
            elementProcessed = false;

            /* Step 02: Check for documentation */
            doxElementDescriptor = new DoxygenElementDescription();
            isStartString = doxElementDescriptor.IsParseStringStartObject(workingString);
            if (true == isStartString)
            {
              elementProcessed = true;
              processedChars = doxElementDescriptor.ParseString(workingString);

              /* Remove documentation information from working string */
              workingString = workingString.Remove(0, processedChars);
            }

            /* Step 03: Parse variable elements */
            isStartString = IsParseStringStartFunctionObject(workingString);
            if (true == isStartString)
            {
              /* Check if doxygen comment is available */
              if (true == elementProcessed)
              {
                processedChars = ParseFunctionString(workingString, -1, doxElementDescriptor);
              }
              else
              {
                processedChars = ParseFunctionString(workingString, -1, null);
              }


              /* Remove element information from working string */
              processedChars++; // Remove also the additional '\n' after each variable
              workingString = workingString.Remove(0, processedChars);
              elementProcessed = true;
            }

            /* Check of one element has been processed */
            if (true == elementProcessed)
            {
              elementProcessed = true;
            }
            else
            {
              elementProcessed = false;
            }
          }
        }
      }

      /* Check if parsing was successful */
      if (0 == workingString.Length)
      { parseSuccessful = true; }
      else
      { parseSuccessful = false; }

      return parseSuccessful;
    }

    /**
      * @brief      Interface function to check if selected element is start element of the string
      * @param      string to be checked
      * @retval     bool >> true = element is start element of string
      * @note       A direct detection if the object ist the actual start element is not possible. So an
      *             indirect detection must be used.
      */
    public bool IsParseStringStartFunctionObject(string str)
    {
      bool retVal = true;
      string searchPattern;
      int indexOfSearchPattern;

      /* >>> Check for an include <<< */
      searchPattern = "#include \"";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);

      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = false;
      }

      /* >>> Check for an define <<< */
      searchPattern = "#define ";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);

      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = false;
      }

      /* >>> Check for an Comment start <<< */
      searchPattern = "/*";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);

      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = false;
      }

      /* >>> Check for an Comment start <<< */
      searchPattern = "//";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);

      if (0 == indexOfSearchPattern)
      {
        /* Element Found at start pos*/
        retVal = false;
      }

      return retVal;
    }

    /**
      * @brief      Interface function to start the string parser for an function string.
      * @param      string to be parsed
      * @param      int group id
      * @param      DocumentationObject variableDescription
      * @retval     Count of chars which has been parsed
      */
    private int ParseFunctionString(string str, int groupId, DocumentationObject variableDescription)
    {
      string searchPattern, workingString, workingString2;
      int indexOfSearchPattern, processedChars = 0, idx = 0, idx2 = 0;
      char[] splitPattern = { ' ' };
      string[] stringElements, stringElements2;

      string returnType = "", fncName = "", fncPara = "", fncContent = "";

      /* Extract function header */
      searchPattern = ")\n{\n";
      indexOfSearchPattern = str.IndexOf(searchPattern, 0, str.Length, StringComparison.Ordinal);
      workingString = str.Substring(0, indexOfSearchPattern);
      workingString2 = str.Remove(0, indexOfSearchPattern + searchPattern.Length);
      processedChars += searchPattern.Length + indexOfSearchPattern;

      /* Parse function header */
      /* Split elements */
      stringElements = workingString.Split(splitPattern);

      /* Process function return type */
      returnType = stringElements[idx++];

      /* Process function name */
      stringElements2 = stringElements[idx++].Split('(');
      fncName = stringElements2[0];

      /* Process parameter list */
      for(idx2 = idx; idx2 < stringElements.Length; idx2++ )
      {
        if ("" == fncPara)
        {
          fncPara += stringElements[idx2];
        }
        else
        {
          if("" != stringElements[idx2])
          {
            fncPara += " " + stringElements[idx2];
          }
        }
      }

      /* Process content */
      searchPattern = "\n}\n";
      indexOfSearchPattern = workingString2.IndexOf(searchPattern, 0, workingString2.Length, StringComparison.Ordinal);
      workingString2 = workingString2.Remove(indexOfSearchPattern);
      processedChars += indexOfSearchPattern + searchPattern.Length;

      fncContent = workingString2;

      /* Add function information */
      if (-1 == groupId)
      {
        if (null == variableDescription)
        {
          AddFunction(returnType, fncName, fncPara, fncContent);
        }
        else
        {
          AddFunction(returnType, fncName, fncPara, variableDescription, fncContent);
        }
      }
      else
      {
        if (null == variableDescription)
        {
          AddFunction(returnType, fncName, fncPara, groupId, fncContent);
        }
        else
        {
          AddFunction(returnType, fncName, fncPara, groupId, variableDescription, fncContent);
        }
      }

      return processedChars;
    }

  }//end CFileFunctionObjects

}//end namespace CFileObjects