/******************************************************************************
 * @file      version.cs
 * @author  
 * @proj      OsarSourceFileLib
 * @date      Saturday, December 12, 2020
 * @version   Application v. 1.1.9.0
 * @version   Generator   v. 1.2.3.9
 * @brief     Controls the assembly Version
 *****************************************************************************/
using System;
using System.Reflection;

[assembly: AssemblyVersion("1.1.9.0")]

namespace OsarSourceFileLib
{
  public static class OsarSourceFileLibVersionClass
	{
		public static int major { get; set; }	 //Version of the programm
		public static int minor { get; set; }	 //Sub version of the programm
		public static int patch { get; set; }	 //Debug patch of the orgramme
		public static int build { get; set; }	 //Count programm builds
		
    static OsarSourceFileLibVersionClass()
    {
			major = 1;
			minor = 1;
			patch = 9;
			build = 0;
    }

		public static string getCurrentVersion()
    {
      return major.ToString() + '.' + minor.ToString() + '.' + patch.ToString() + '.' + build.ToString();
    }
	}
}
//!< @version 0.1.0	->	Inital implementation of OSAR source file library. Including following elements: Generation of includes / defines / variables / functions / dataTypes / doxygen comments in header and source files.
//!< @version 0.1.1	->	Adding OSAR MemMap define functions to the general goup definition objects.
//!< @version 0.1.2	->	Bugfix: Now copy generic documentation objects insted of referencing the object.
//!< @version 0.1.3	->	Add new rearrange of struct arrays.
//!< @version 1.0.0	->	Adding support of Module Makefile generation.
//!< @version 1.0.1	->	Adding "Universal Unique ID" field in the Doxygen Element Description class.
//!< @version 1.0.2	->	Add parsing of functions / variables  /  includes / Doxygen Element Descriptor / Doxygen File Header  and grouping elements to OSAR Source File Library
//!< @version 1.1.0	->	Adding full parsing functionality within the OSAR Source file Lib. Different elements could be parsed and requested by the APIs. Additional Element Getter functions are still needed.
//!< @version 1.1.1	->	Modify doxygen file group opener and closer. Adding master group functionality. Remove define group functionality.
//!< @version 1.1.2	->	Adding interfaces to set user defined code section area strings. Adapting "IsStartElement..." interfaces in their visibility.
//!< @version 1.1.3	->	Rearrange the automatic string rearrangement of the doxygen element description. Now user \n could be processed as well.
//!< @version 1.1.4	->	Implementation of function prototype generation bugfix. >> Now also grouping information would be generated.
//!< @version 1.1.5	->	Fix spelling error in makefile generation.
//!< @version 1.1.6	->	Adapting the user code implementatiion start and end comments
//!< @version 1.1.7	->	Adding bugfix for generation of grouped function prototypes.
//!< @version 1.1.8	->	Implemented bugfix for parsing source file. Replace now \r\n with only \n which is used during generation.
//!< @version 1.1.9	->	Add bugfix while parsing doxygen group opener objects. Add parsing of custom group starter / end comments.
