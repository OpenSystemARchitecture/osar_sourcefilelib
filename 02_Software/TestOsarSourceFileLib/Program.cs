﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarSourceFileLib;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.ModuleMakefile;
using OsarSourceFileLib.DocumentationObjects.General;

namespace TestOsarSourceFileLib
{
  class Program
  {
    static string dummyModuleName = "DemoModule";

    static void createDummyCFile()
    {
      string fileName = "dummyCfile.c";

      //Setup basic file informations
      CSourceFile dummyCFile = new CSourceFile();
      dummyCFile.AddFileName(fileName);
      dummyCFile.AddFilePath("C:\\temp\\TestProg");
      dummyCFile.AddUserCodeSections();

      //Add an doxygen file header
      /* --------------------------------------------- Add an doxygen file header --------------------------------------------------- */
      DoxygenFileHeader doxFileHeader = new DoxygenFileHeader();
      doxFileHeader.AddFileName(fileName);
      doxFileHeader.AddFileAuthor("OSAR Team S.Reinemuth");
      doxFileHeader.AddFileBrief("Demostration dummy c source file.\n");
      doxFileHeader.AddFileDetails("This c file has been generated from the OSAR Source file library \nfor\ndemonstrating the functionality of this generator. \r\n\r\nThis is only for demostration purpose. To it has also to be tested if an line brake could be processed. So lets see\n");
      doxFileHeader.AddFileNote("!!! For demonstration purpose only !!!");
      doxFileHeader.AddFileVersion("1.2.3.4");
      dummyCFile.AddFileCommentHeader(doxFileHeader);

      /* --------------------------------------------- Add an doxygen file grouping  --------------------------------------------------- */
      DoxygenFileGroupOpener doxFileGroupOpener = new DoxygenFileGroupOpener();
      //doxFileGroupOpener.AddToDoxygenMasterGroup("dummyMaster");
      doxFileGroupOpener.AddToDoxygenGroup("dummyModuleName");
      DoxygenFileGroupCloser doxFileGroupCloser = new DoxygenFileGroupCloser();
      dummyCFile.AddFileGroup(doxFileGroupOpener, doxFileGroupCloser);


      /* --------------------------------------------- Add an gloabl function --------------------------------------------------- */
      int functionGroup = 0;
      string test;
      CFileFunctionObjects dummyGlobalFunctions = new CFileFunctionObjects();
      DoxygenElementDescription functionElementDesc = new DoxygenElementDescription();
      GeneralStartGroupObject functionGroupOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject functionGroupCloser = new GeneralEndGroupObject();

      functionGroupOpener.AddOsarStartMemMapDefine("Dummy", OsarMemMapDataType.OSAR_CODE);
      functionGroupOpener.AddGroupName("Start MemMap for Code");
      functionGroupCloser.AddOsarStopMemMapDefine("Dummy", OsarMemMapDataType.OSAR_CODE);

      functionElementDesc.AddElementBrief("DummyFunction");
      functionElementDesc.AddElementParameter("dummyData", CParameterType.INPUT);
      functionElementDesc.AddElementParameter("pData", CParameterType.OUTPUT);
      functionElementDesc.AddElementReturnValues("return value test string");
      functionElementDesc.AddElementDetails("details\n\ntest\nsting");
      functionElementDesc.AddElementNote("lool alsdfj alskdfj oaidfj aldkfj aoidfj laskdfj \ndifj\nlasdf alskdfj di adsfja sd dfjalskdf alskdfj alsdk skfj sla sdkf asldkfj asdkfj asdkf alskdfj alsdkf asdkfj asldfk asdflk asdfjalsdkfj asdkfj alsdkfjjadsf lasdfkj adfkkaf aldfk adskfj asdfkj ");
      test = functionElementDesc.GenerateElementUUID();

      functionGroup = dummyGlobalFunctions.AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);

      dummyGlobalFunctions.AddFunction("void", "dummyFunction", "uint8 data, uint8* pData", functionGroup, functionElementDesc, "uint8 idx = 0");
      dummyGlobalFunctions.AddFunction("void", "dummyFunction2", "uint8 data, uint8* pData", functionGroup, "uint8 idx = 0");
      dummyCFile.AddGlobalFunctionObject(dummyGlobalFunctions);

      /* --------------------------------------------- Add an include object --------------------------------------------------- */
      CFileIncludeObjects includeObject = new CFileIncludeObjects();
      functionGroup = includeObject.AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);
      includeObject.AddIncludeFile("Test1.h", functionGroup, functionElementDesc);
      includeObject.AddIncludeFile("Test2.h", functionGroup, functionElementDesc);
      includeObject.AddIncludeFile("Test3.h", functionGroup);
      includeObject.AddIncludeFile("Test4.h", functionElementDesc);
      includeObject.AddIncludeFile("Test5.h");
      dummyCFile.AddIncludeObject(includeObject);

      /* --------------------------------------------- Add an private function --------------------------------------------------- */
      CFileFunctionObjects dummyPrivateFunctions = new CFileFunctionObjects();
      functionGroup = dummyPrivateFunctions.AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);
      dummyPrivateFunctions.AddFunction("void", "privateDummyFunction1", "uint8 data, uint8* pData", functionGroup, functionElementDesc, "uint8 idx = 0");
      dummyPrivateFunctions.AddFunction("void", "privateDummyFunction2", "uint8 data, uint8* pData", functionGroup, "uint8 idx = 0");
      dummyCFile.AddPrivateFunctionPrototypeObject(dummyPrivateFunctions);
      dummyCFile.AddPrivateFunctionObject(dummyPrivateFunctions);

      /* --------------------------------------------- Add an global variable --------------------------------------------------- */
      CFileVariableObjects dummyGlobalVariables = new CFileVariableObjects();
      dummyGlobalVariables.AddVariable("uint8", "dummyVar1");
      dummyGlobalVariables.AddVariable("uint8", "dummyVar2","","extern");
      dummyGlobalVariables.AddVariable("uint8", "dummyVar3","0xAA", "const");
      dummyGlobalVariables.AddVariable("uint8", "dummyVar4", functionElementDesc);
      functionGroup = dummyGlobalVariables.AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);
      dummyGlobalVariables.AddVariable("uint8", "dummyVar5", functionGroup);
      dummyGlobalVariables.AddVariable("uint8", "dummyVar6", functionGroup, functionElementDesc);
      dummyCFile.AddGlobalVariableObject(dummyGlobalVariables);

      /* --------------------------------------------- Add private Arrays  --------------------------------------------------- */
      CFileVariableObjects dummyPrivateArray = new CFileVariableObjects();
      dummyPrivateArray.AddArray("uint8", "dummyArray1", "100");
      dummyPrivateArray.AddArray("uint8", "dummyArray2", "100", "", "const");
      dummyPrivateArray.AddArray("uint8", "dummyArray1", "100", "0x01, 0x02, 0x03, 0x004, 0x0005, 0x00000006, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C," +
        "0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29," +
        "0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39");
      dummyPrivateArray.AddArray("uint8", "dummyArray1", "100", functionElementDesc);
      functionGroup = dummyPrivateArray.AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);
      dummyPrivateArray.AddArray("uint8", "dummyArray1", "100", functionGroup, "0x01, 0x02, 0x03, 0x004, 0x0005, 0x00000006, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C," +
        "0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29," +
        "0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39");
      dummyPrivateArray.AddArray("uint8", "dummyArray1", "100", functionGroup, functionElementDesc, "0x01, 0x02, 0x03, 0x004, 0x0005, 0x00000006, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C," +
        "0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29," +
        "0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39 ", "const");
      dummyPrivateArray.AddArray("uint8", "dummyArray1", "100", "{0x01, 0x02, 0x03, 0x004}, {0x05, 0x06, 0x07, 0x008},{0x09, 0x0A, 0x0B, 0x00C},{0x0D, 0x0E, 0x0F, 0x010}", "const");

      dummyCFile.AddPrivateVariableObject(dummyPrivateArray);

      // Generate C Source file
      dummyCFile.GenerateSourceFile();
    }

    static void createDummyHFile()
    {
      string fileName = "dummyHfile.h";

      //Setup basic file informations
      CHeaderFile dummyHFile = new CHeaderFile();
      dummyHFile.AddFileName(fileName);
      dummyHFile.AddFilePath("C:\\temp\\TestProg");
      dummyHFile.AddUserCodeSections();

      //Add an doxygen file header
      DoxygenFileHeader doxFileHeader = new DoxygenFileHeader();
      doxFileHeader.AddFileName(fileName);
      doxFileHeader.AddFileAuthor("OSAR Team S.Reinemuth");
      doxFileHeader.AddFileBrief("Demostration dummy h source file.");
      doxFileHeader.AddFileDetails("This c file has been generated from the OSAR Source file library for demonstrating the functionality of this generator.");
      doxFileHeader.AddFileNote("!!! For demonstration purpose only !!!");
      doxFileHeader.AddFileVersion("1.2.3.4");
      dummyHFile.AddFileCommentHeader(doxFileHeader);

      //Add an doxygen File Grouping
      DoxygenFileGroupOpener doxFileGroupOpener = new DoxygenFileGroupOpener();
      doxFileGroupOpener.AddToDoxygenMasterGroup("dummyMaster");
      doxFileGroupOpener.AddToDoxygenGroup("dummyModuleName");
      DoxygenFileGroupCloser doxFileGroupCloser = new DoxygenFileGroupCloser();
      dummyHFile.AddFileGroup(doxFileGroupOpener, doxFileGroupCloser);

      //Add Includes
      int firstIncludeGroup;
      CFileIncludeObjects includes = new CFileIncludeObjects();
      GeneralStartGroupObject includeStartGroup1 = new GeneralStartGroupObject();
      GeneralEndGroupObject includeEndGroup1 = new GeneralEndGroupObject();
      DoxygenElementDescription includeDescription1 = new DoxygenElementDescription();
      DoxygenElementDescription includeDescription2 = new DoxygenElementDescription();

      //includeStartGroup1.AddGroupName("Standard Demo Includes");
      //includeStartGroup1.AddCustomStartGroupString("/* This is the custom start group element. It could be used to perform the memory mapping! */");
      includeEndGroup1.AddCustomEndGroupString("/* This is the custom end group element. It could be used to perform the memory mapping! */");
      firstIncludeGroup = includes.AddCFileObjectGroup(includeStartGroup1, includeEndGroup1);

      includeDescription1.AddElementBrief("Standard data type header");
      includeDescription2.AddElementBrief("Standard platforms types");

      includes.AddIncludeFile("Std_Types.h", includeDescription1);
      includes.AddIncludeFile("Plattform_Types.h", includeDescription2);
      includes.AddIncludeFile("DummyModuleHeader.h", firstIncludeGroup);

      dummyHFile.AddIncludeObject(includes);


      //Add a compiler definition
      int definitionGroupIdx;
      CFileDefinitionObjects definitions = new CFileDefinitionObjects();
      DoxygenElementDescription firstdefinitionDescription = new DoxygenElementDescription();
      firstdefinitionDescription.AddElementBrief("Demo definition");
      firstdefinitionDescription.AddElementNote("Only for demostration purpose.");
      definitions.AddDefinition("USED_DUMMY_MODULE", "Std_On /* Beacuse it is awsome */", firstdefinitionDescription);
      definitions.AddDefinition("USED_DUMMY_MODULE_TEST2", "Std_On /* Beacuse it is awsome */");

      definitions.AddDefinition("SECOND_TEST_OF_DEFINITION_WITH_DIFFERENT_LENGTH", "0x112233");
      definitions.AddDefinition("THIRD_TEST", "1");

      dummyHFile.AddDefinitionObject(definitions);

      //Add an function
      int functionGroup = 0;
      CFileFunctionObjects dummyGlobalFunctions = new CFileFunctionObjects();
      DoxygenElementDescription functionElementDesc = new DoxygenElementDescription();
      GeneralStartGroupObject functionGroupOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject functionGroupCloser = new GeneralEndGroupObject();

      //functionGroupOpener.AddCustomStartGroupString("#define dummyFile_START_SEC_CODE \n#include \"MemMap.h\"\n");
      functionGroupOpener.AddGroupName("Start MemMap for Code");
      //functionGroupCloser.AddCustomEndGroupString("#define dummyFile_STOP_SEC_CODE \n#include \"MemMap.h\"\n");
      functionGroupOpener.AddOsarStartMemMapDefine("dummyFile", OsarMemMapDataType.OSAR_CODE);
      functionGroupCloser.AddOsarStopMemMapDefine("dummyFile", OsarMemMapDataType.OSAR_CODE);

      functionElementDesc.AddElementBrief("DummyFunction");
      functionElementDesc.AddElementParameter("dummyData", CParameterType.INPUT);
      functionElementDesc.AddElementParameter("pData", CParameterType.OUTPUT);
      functionElementDesc.AddElementNote("lool");

      functionGroup = dummyGlobalFunctions.AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);

      dummyGlobalFunctions.AddFunction("void", "dummyFunction", "uint8 data, uint8* pData", functionGroup, functionElementDesc, "uint8 idx = 0");
      dummyGlobalFunctions.AddFunction("void", "dummyFunction2", "uint8 data, uint8* pData", functionGroup, functionElementDesc, "uint8 idx = 0");
      dummyHFile.AddGlobalFunctionPrototypeObject(dummyGlobalFunctions);

      /* --------------------------------------------- Add data types --------------------------------------------------- */
      CFileTypeObjects dummyTypes = new CFileTypeObjects();
      dummyTypes.AddNewDataTypeDefinition("uint8", "dummyU8_1");
      dummyTypes.AddNewDataTypeDefinition("uint8", "dummU8_2", functionElementDesc);
      functionGroup = dummyTypes.AddCFileObjectGroup(functionGroupOpener, functionGroupCloser);
      dummyTypes.AddNewDataTypeDefinition("uint8", "dummU8_3", functionGroup, functionElementDesc);

      string[] enumElements = { "ENUM_1 = 0", "ENUM_2", "ENUM_3", "ENUM_4", "ENUM_5", "ENUM_6", "ENUM_7", "ENUM_8"};
      dummyTypes.AddEnumerationTypeDefinition("dummyEnum1", enumElements);
      dummyTypes.AddEnumerationTypeDefinition("dummyEnum2", enumElements, functionElementDesc);
      dummyTypes.AddEnumerationTypeDefinition("dummyEnum3", enumElements, functionGroup);
      dummyTypes.AddEnumerationTypeDefinition("dummyEnum4", enumElements, functionGroup, functionElementDesc);


      string[] structElements = { "uint8 testData1", "uint8 testData2", "uint8 testData3", "uint8 testData4", "uint8 testData5" };
      dummyTypes.AddStructTypeDefinition("dummyStruct1", structElements);
      dummyTypes.AddStructTypeDefinition("dummyStruct2", structElements, functionElementDesc);
      dummyTypes.AddStructTypeDefinition("dummyStruct3", structElements, functionGroup);
      dummyTypes.AddStructTypeDefinition("dummyStruct4", structElements, functionGroup, functionElementDesc);

      dummyHFile.AddTypesObject(dummyTypes);

      // Generate C Source file
      dummyHFile.GenerateSourceFile();
    }

    static void createDummyModuleMakeFile()
    {
      string fileName = "dummyModuleMakeFile.mk";

      ModuleMakefile moduleMake = new ModuleMakefile();

      moduleMake.AddFileName(fileName);
      moduleMake.AddFilePath("C:\\temp\\TestProg");

      moduleMake.AddMakefileBasePath("BSW");
      moduleMake.AddMakefileModuleName("Test");
      //moduleMake.AddMakefileSourceFileName("02_Generated\\src\\test.c");
      //moduleMake.AddMakefileSourceFileName("02_Generated\\src\\test2.c");

      //moduleMake.AddMakefileIncludePath("02_Generated\\inc");

      moduleMake.GenerateSourceFile();

    }

    static void Main(string[] args)
    {
      //##### Dymmy File Creation #####
      //createDummyModuleMakeFile();
      //createDummyCFile();
      //createDummyHFile();


      /*##### C-File Parser #####*/
      string fileName = "dummyCfile.c";

      //Setup basic file informations
      CSourceFile dummyCFile = new CSourceFile();
      dummyCFile.AddFileName(fileName);
      dummyCFile.AddFilePath("C:\\temp\\TestProg");

      dummyCFile.ParseSourceFile();

      string dummyUserIncludeContent = dummyCFile.GetUserIncludeDefinionSectionString();
      string dummyUserFunctionContent = dummyCFile.GetUserFunctionSectionString();


      /*##### H-File Parser #####*/
      //string fileName = "dummyHfile.h";

      ////Setup basic file informations
      //CHeaderFile dummyHFile = new CHeaderFile();
      //dummyHFile.AddFileName(fileName);
      //dummyHFile.AddFilePath("C:\\temp\\TestProg");

      //dummyHFile.ParseSourceFile();

    }
  }
}
