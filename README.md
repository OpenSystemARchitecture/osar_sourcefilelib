# OSAR_SourceFileLib

## General:
The OSAR Source file library could be used to generate a standardized programming source files. The OSAR source file library itself is designed in separate modules with standardized interfaces. So, the user could adapt or expand the library with user elements.

## Abbreviations:
OSAR == Open System ARchitecture

## Useful Links:
- Overall OSAR-Artifactory: http://riddiks.ddns.net:8082/artifactory/webapp/#/artifacts/browse/tree/General/prj-open-system-architecture
- OSAR - SourceFileLib releases: http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_SourceFileLib/